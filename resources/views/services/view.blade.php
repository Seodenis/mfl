@extends('layouts.app')

@section('meta_title'){{ $item->meta_title }}@endsection
@section('meta_description'){{ $item->meta_description }}@endsection

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('services.item', $item) }}


        <h1>{{ $item->title }}</h1>
        @if($item->introtext)
            {!! $item->introtext !!}
        @endif

        <div class="price-article-head flex between align-top">
            <div class="left text width1-2">
                @if($item->price)
                    <p class="price">Цена: {{ $item->price }} рублей</p>
                @endif
                {!! $item->description !!}
            </div>
            @widget('FeedBack', ['title' => 'Напишите свой вопрос, и наши юристы помогут', 'textarea' => true])
        </div>

        <div class="text margin-top">
            {!! $item->text !!}
            {!! $item->bottom_text !!}
        </div>

    </div>
@endsection
