@extends('layouts.app')

@section('meta_title'){{ $item->meta_title }}@endsection
@section('meta_description'){{ $item->meta_description }}@endsection

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('services.item', $item) }}

        <h1>{{ $item->title }}</h1>

        {{--@if($item->introtext)
            {!! $item->introtext !!}
        @endif--}}

        <div class="price-article-head flex between align-top">
            <div class="video shadow width1-2">
                @if($item->show_video)
                    <iframe
                        width="560" height="315" src="https://www.youtube.com/embed/{{ $item->view_video }}?controls=0"
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                    <p class="video-undertitle">{{ $item->view_video_text }}</p>
                @else
                    <img src="{{ front_storage_path($item->view_image) }}" width="560" title="{{ $item->title }}">
                @endif
            </div>
            @widget('FeedBack', ['title' => 'Напишите свой вопрос, и наши юристы помогут', 'textarea' => true])
        </div>

        <div class="text margin-top">
            {!! $item->text !!}
            {!! $item->bottom_text !!}
        </div>

    </div>
@endsection
