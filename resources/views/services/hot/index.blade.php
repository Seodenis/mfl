@extends('layouts.app')

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('services.hot') }}


        <h1>{{ __('Прайс-лист') }}</h1>
        @if($items)
            <div class="hot-services flex between wrap">
                @foreach($items as $k => $v)

                    <div class="item whitebg padding30-20 width1-2">
                        <div class="top flex between align-center">
                        @if($v->image)
                            <div class="image">
                                <img src="{{ front_storage_path($v->image) }}" alt="{{ $v->title }}">
                            </div>
                        @endif
                            <div class="right flex start wrap">
                                <a class="title" href="/{{ $v->uri }}" class="title">{{ $v->title }}</a>
                                <a class="readmore" href="/{{ $v->uri }}">Подробнее</a>
                            </div>
                        </div>
                        {!! description($v->description, $v->text) !!}

                    </div>
                @endforeach

                {{ $items->render() }}
            </div>
        @endif
    </div>
@endsection
