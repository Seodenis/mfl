@extends('layouts.app')

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('services.index') }}


        <h1>{{ __('Прайс-лист') }}</h1>
        @if($items)
            <div class="price-block whitebg padding30-20">
                @foreach($items as $k => $v)

                    <div class="price-item flex between align-center">

                        @if($v->image)
                            <div class="image">
                                <img src="{{ front_storage_path($v->image) }}" alt="{{ $v->title }}">
                            </div>
                        @endif
                        <div class="service">
                            <a href="/{{ $v->uri }}" class="title">{{ $v->title }}</a>
                            <div class="desc">{!! $v->introtext !!} [<a href="/{{ $v->uri }}">?</a>]
                            </div>
                        </div>
                        <div class="price">
                            @if($v->price)
                                <p>{{ __('Цена') }}:
                                    <span>{{ $v->price }} рублей</span>
                                </p>
                            @endif
                            <a href="/{{ $v->uri }}">Заказать услугу</a>
                        </div>
                    </div>
                @endforeach

                {{ $items->render() }}
            </div>
        @endif
    </div>
@endsection
