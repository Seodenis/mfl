@extends('layouts.app')

@section('content')
    <h3>{{ $item->title }}</h3>
    @if($item->price)
        {{ $item->price->value }} {{ _('рублей') }}
    @endif
    <div>{!! $item->description !!}</div>
    <div>{!! $item->content !!}</div>
    <div>{!! $item->bottom_content !!}</div>
@endsection
