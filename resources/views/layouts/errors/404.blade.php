@extends('layouts.app')

@section('content')
    <div class="wrapper_flex flex between align_top">
        <div class="pseudo_left_panel_box"></div>
        <div class="content">
            <div class="container" align="center">
                <h3>404 Error<br>Страницы не существует!</h3>
            </div>
        </div>
    </div>
@endsection
