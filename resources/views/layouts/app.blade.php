<?php $noCache = app('env') == 'local' ? '?' . time() : false; ?>

<?php

if ( ! isset($site)) {
    $const = sub_domain() ? sub_domain() : 'web';

    $site = \App\Site::where('const', $const)->first();
    $metaData = [];
}
?>
    <!DOCTYPE html>
<html dir="ltr" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if($metaData && $metaData->title){{ $metaData->title }}@else @yield('meta_title', $site->meta_title)@endif</title>

    <meta
        name="description"
        content="@if($metaData && $metaData->description){{ $metaData->description }}@else @yield('meta_description', $site->meta_description)@endif">
    {{-- Canonical --}}
    <link rel="canonical" href="{{ url()->current() }}"/>

    <link rel="stylesheet" href="{{ asset('css/main.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}{{ $noCache }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}{{ $noCache }}" type="text/css">
    <link
        rel="stylesheet" href="{{ asset('vendor/kladrapi-jsclient-master/jquery.kladr.min.css') }}{{ $noCache }}"
        type="text/css">
    <link rel="stylesheet" href="{{ asset('css/init.css') }}" type="text/css"/>

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken'=> csrf_token(),
            'user'=> [
                'authenticated' => auth()->check(),
                'id' => auth()->check() ? auth()->user()->id : null,
                'name' => auth()->check() ? auth()->user()->name : null,
                ]
            ])
        !!};
    </script>

</head>

<body class="mainpage">

<div id="preloader">
    <img src="/images/preloader.gif">
</div>

<div class="wrapper">

    @if (session('message'))
        <div class="success">{!! session('message') !!}</div>
    @endif

    @if (session('status'))
        <div class="success">{!! session('status') !!}</div>
    @endif

    @if(isset($errors) && $errors->has('error'))
        <div class="success">{{ $errors->first('error') }}</div>
    @endif

    <div class="container-wrap">
        <div class="mobpanel flex between align-center">
            <a class="logo" href="{{ url('/') }}">
                <img src="{{ asset('images/logo.png') }}">
            </a>
            <a href="tel:{{ $site->phone }}">
                <i class="fa fa-phone"></i>
                <span class="roistat-phone-tel">{{ $site->phone }}</span>
            </a>
            <div class="menu-btn">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>


        <header class="shadow">
            <div class="top">
                <div class="container flex between align-center">
                    <div class="left adress">{{ $site->address }} &nbsp; [
                        <a target="_blank" href="https://yandex.ru/maps/?text={{ $site->address }}">
                            <i class="fa fa-map-marker"></i>
                            карта
                        </a>
                        ]
                    </div>
                    <div class="right  flex between align-center">
                        <div class="menu">
                            @widget('Menu', ['type' => 'site_top'])
                        </div>
                        @guest

                            <div class="login">
                                <i class="fa fa-user"></i>
                                <a href="{{ route('login') }}" class="popup-ajax-link">{{ __('Войти') }}</a>
                            </div>

                            <div class="login">
                                <i class="fa fa-user"></i>
                                <a href="{{ route('register') }}" class="popup-ajax-link">{{ __('Регистрация') }}</a>
                            </div>
                        @else


                            <div class="authorized_header flex between align-center">
                                <div class="user_avatar">
                                    <img
                                        class="profile-img" src="{{ avatarPath(Auth::user()->avatar) }}"
                                        alt="{{ Auth::user()->name }}">
                                </div>
                                <div class="name_and_menu">
                                    <div class="user_name">
                                        <span>{{ __('Профиль') }}</span>
                                        <i class="fa fa-angle-down"></i>
                                    </div>
                                    <ul class="user_menu">
                                        <li>
                                            <a href="{{ route('profile.requests') }}">
                                                {{ __('Заявки') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('requests.add', ['type' => 'chat', 'save' => true]) }}">
                                                {{ __('Оставить заявку') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('profile.settings') }}">
                                                {{ __('Настройки') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a
                                                href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                {{ __('Выйти') }}
                                            </a>
                                        </li>

                                        <form
                                            id="logout-form" action="{{ route('logout') }}" method="POST"
                                            style="display: none;">
                                            @csrf
                                        </form>

                                    </ul>
                                </div>
                            </div>
                        @endguest

                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container flex between align-center">
                    <a class="logo" href="/">
                        <img src="{{ asset('images/logo.png') }}">
                    </a>
                    <div class="menu">
                        @widget('Menu', ['type' => 'site_top_header'])
                    </div>
                    <div class="contacts-block flex between align-center">
                        <a href="tel:{{ $site->phone }}">
                            <i class="fa fa-phone"></i>
                            <span class="roistat-phone-tel">{{ $site->phone }}</span>
                        </a>
                        <a
                            href="{{ route('requests.add', ['type' => 'PHONE_CALL']) }}"
                            class="green btn callback popup-ajax-link">Перезвоните мне
                        </a>
                    </div>
                </div>
            </div>
        </header>

        @if (session('status'))
            <div class="alert alert-primary" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @if (session('message'))
            <div class="alert alert-primary" role="alert">
                {{ session('message') }}
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif

        @if (isset($errors) && $errors->has('error'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('error') }}
            </div>
        @endif

        @yield('content')

        <div class="container contacts margin-top">
            <h2>
                <span>Остались вопросы?</span>
            </h2>
            <div class="items flex between align-center">
                <div class="width1-2 left">
                    <p class="big">Позвоните нам</p>
                    <a href="tel:{{ $site->phone }}"><span class="roistat-phone-tel">{{ $site->phone }}</span></a>
                    <p class="little">{{ __('и мы поможем вам уже сегодня') }}</p>
                </div>
                @widget('FeedBack', ['title' => 'Нет времени разбираться? Оставьте свои контакты, и мы свяжемся с вами'])
            </div>
        </div>

    </div>

    <footer>
        <div class="container items flex between align-top">
            @widget('Menu', ['type' => 'site_bottom', 'positionType' => 'bottom'])
            <div class="item width1-5 right">
                <a class="logo" href="/">
                    <img src="{{ asset('/images/logo.png') }}">
                </a>
                <p>
                    <a href="tel:{{ $site->phone }}"><span class="roistat-phone-tel">{{ $site->phone }}</span></a>
                    <a target="_blank" href="https://www.instagram.com/moj.yurist">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a target="_blank" href="http://www.youtube.com/c/МойСемейныйЮристМосква">
                        <i class="fa fa-youtube"></i>
                    </a>
                    <a target="_blank" href="http://vk.com/moj.yurist">
                        <i class="fa fa-vk"></i>
                    </a>
                    <a target="_blank" href="https://telegram.me/myfamilylawyer">
                        <i class="fa fa-telegram"></i>
                    </a>
                    <a target="_blank" href="https://www.facebook.com/moj.yurist/">
                        <i class="fa fa-facebook-square"></i>
                    </a>
                </p>
                <p>{{ $site->address }}</p>
            </div>
        </div>
        <p>Для жалоб и предложений обращайтесь
            <a href="mailto:{{ setting('site.complaints-and-suggestions') }}">{{ setting('site.complaints-and-suggestions') }}</a>
        </p>
        <p>2013 - {{ date('Y') }} АО "Семейный Юрист". Все права защищены</p>
    </footer>
</div>

<script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
@yield('javascript')

<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/jquery.fancybox.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/common.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/moment.min.js') }}{{ $noCache }}"></script>
<script type="text/javascript" src="{{ asset('js/daterangepicker.min.js') }}{{ $noCache }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.ui.datepicker-ru.js') }}{{ $noCache }}"></script>
<script
    src="{{ asset('vendor/kladrapi-jsclient-master/jquery.kladr.min.js') }}{{ $noCache }}"
    type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('js/init.js') }}"></script>

@yield('innerScripts')

@if(config('app.env') == 'production')
    @include('partials.counters')
@endif

</body>
</html>
