@extends('layouts.app')

@section('meta_title'){{ $items->first()->meta_title }}@endsection
@section('meta_description'){{ $items->first()->meta_description }}@endsection

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('article.data', 'contacts', $items->first()) }}

        <h1>{{ __('Контакты') }}</h1>

        <div class="text contacts-page">

            <div class="contacts-page-head flex between align-top">
                <div class="width1-2 flex between wrap">
                    <div class="width1-2">
                        <h3>{{ __('Позвонить') }}</h3>
                        <p>
                            <a href="tel:{{ $site->phone }}"><span class="roistat-phone-tel">{{ $site->phone }}</span></a>
                        </p>
                    </div>
                    <div class="width1-2">
                        <h3>{{ __('Написать на E-mail') }}</h3>
                        <p>
                            <a href="mailto:{{ $site->email }}">{{ $site->email }}</a>
                        </p>
                    </div>
                    <div class="width1-1">
                        <h3>{{ __('Написать в Whats\'app, Viber, Telegram') }}</h3>
                        <p>
                            <a href="tel:{{ $site->messanger_phone }}">{{ $site->messanger_phone }}</a>
                        </p>
                    </div>
                    <div class="width1-1">
                        <h3>{{ __('Мы в Skype') }}</h3>
                        <p>{{ $site->skype }}</p>
                    </div>
                </div>

                @widget('FeedBack', ['title' => 'Либо оставьте свои контакты'])

            </div>

            <h2>
                <span>{{ __('Как нас найти?') }}</span>
            </h2>

            <p>{{ $site->address }}</p>

            <script
                type="text/javascript" charset="utf-8" async
                src="{{ $site->map_link }}"></script>

            <div class="green-titles">{!! $site->map_description !!}</div>

            <h2>
                <span>{{ __('Вид с улицы') }}</span>
            </h2>
            <img src="{{ front_storage_path($site->street_view_image) }}" alt="" style="float:left; margin-right:20px;">
            <br>
            {!! $site->street_view_description !!}

            <div class="clr"></div>

            <h2>
                <span>{{ __('Реквизиты') }}</span>
            </h2>
            {!! $site->requisites !!}
        </div>
    </div>

@endsection
