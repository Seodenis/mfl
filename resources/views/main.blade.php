@extends('layouts.app')

@section('content')
    @widget('Banners', ['type' => 'main_page_top'])

    <div class="welcome container margin-top flex between align-center">
        {!! $site->main_top_description !!}
    </div>

    @widget('Specializations')

    @widget('MassMedia')

    @widget('WonCases')

    @widget('Gratitude')

    @widget('DigitalInfo')

    @widget('Partners')

    <div class="container about margin-top">
        {!! $site->main_bottom_description !!}
    </div>
@endsection
