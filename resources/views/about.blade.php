@extends('layouts.app')

@section('meta_title'){{ $items->first()->meta_title }}@endsection
@section('meta_description'){{ $items->first()->meta_description }}@endsection

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('article.data', 'about', $items->first()) }}

        @if($items)
            @foreach($items as $k => $v)
                <h1>{{ $v->title }}</h1>
                <div class="text">
                    <div>{!! $v->text !!}</div>
                </div>
            @endforeach
        @endif
    </div>
@endsection
