<div class="container contacts-horizontal margin-top">
    <div class="items flex between align-center">
        <div class="width1-2 left">
            <h2><span>Напишите свой вопрос,<br>и наши юристы помогут</span></h2>
        </div>
        @widget('FeedBack', ['title' => false, 'textarea' => true])
    </div>
</div>
