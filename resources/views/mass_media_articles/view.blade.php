@extends('layouts.app')

@section('meta_title'){{ $item->meta_title }}@endsection
@section('meta_description'){{ $item->meta_description }}@endsection

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('mass_media_articles.item', $item) }}

        <div class="news-article">
            <div class="news-article-head flex between align-center">
                <div class="left width2-3">
                    <h1>{{ $item->title }}</h1>
                    <div class="undertitle-article-block flex start align-center">
                        <div class="date" style="margin: 0 20px 0 0">
                            <i class="fa fa-calendar"></i>
                            <span>{{ $item->created_at->format('d.m.Y') }}</span>
                        </div>
                        @if($item->mass_media)
                            @if($item->mass_media->image)
                                <img src="{{ front_storage_path($item->mass_media->image) }}">
                            @endif
                        @endif
                    </div>
                </div>
                <div class="right width1-3">
                    @if($item->image)
                        <div class="image">
                            <img src="{{ front_storage_path($item->image) }}">
                        </div>
                    @endif
                </div>
            </div>
            <div class="text">
                {!! $item->text !!}
                {!! $item->bottom_text !!}
            </div>

            @widget('MassMediaOther', ['notId' => $item->id])

        </div>
    </div>
@endsection
