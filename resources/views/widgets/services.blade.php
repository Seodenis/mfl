<div id="tabs">
    <ul>
        @foreach($parents as $k => $v)
            <li><a href="#tabs-{{ $k }}">{{ $v->title }}</a></li>
        @endforeach
    </ul>
    @foreach($parents as $k => $v)
        <div id="tabs-{{ $k }}">
            @foreach(\App\Widgets\Services::getItemsByParent($v->id) as $key => $value)
                <h3>{{ $value->title }}</h3>
            @endforeach
        </div>
    @endforeach
</div>

@section('javascript')
    <script>
        $(function () {
            $("#tabs").tabs().addClass("ui-tabs-vertical ui-helper-clearfix");
            $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
        });
    </script>
@endsection

<style>
    .ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left; width: 12em; }
    .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
    .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
    .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
    .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
</style>