<div class="container dela margin-top">
    <h2>
        <span>Выигранные дела</span>
    </h2>
    <ul class="slider-dela slider-container owl-carousel owl-theme">
        @foreach($items as $k => $v)
            <li class="item flex between align-center">
                <div class="image width1-2">
                    <a onclick="getWonCasesImages({{ $v->images }})" href="javascript:void(0);">
                        <img src="{{ front_storage_path(json_decode($v->images)[0]) }}">
                    </a>
                </div>
                <div class="text width1-2">
                    <div class="title">{{ $v->title }}</div>
                    <div class="problem">{!! $v->text !!}</div>
                </div>
            </li>
        @endforeach
    </ul>
    <div class="goto">
        <a href="{{ route('won_cases.index') }}" class="btn green cases">Перейти к кейсам</a>
    </div>
</div>
