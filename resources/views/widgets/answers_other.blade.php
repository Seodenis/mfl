<div class="other-articles">
    <h2>{{ __('Другие вопросы и ответы') }}</h2>
    <ul>
        @foreach($items as $k => $v)
            <li>
                <a href="/{{ $v->uri }}">{{ $v->title }}</a>
            </li>
        @endforeach
    </ul>
</div>
