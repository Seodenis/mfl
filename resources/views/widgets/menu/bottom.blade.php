@if($items)
    <div class="item width1-5">
        <ul>

            @foreach ($items as $k => $v)
                @php
                    $isActive = null;

                    // Check if link is current
                    if(url($v->link()) == url()->current()){
                        $isActive = 'active';
                    }
                @endphp

                <li class="{{ $isActive }}">
                    <a href="{{ url($v->link()) }}">{{ $v->title }}</a>
                </li>

                @if(($k + 1) % 5 == 0)
                    </ul>
                </div>
                <div class="item width1-5">
                    <ul>
                @endif
            @endforeach

        </ul>
    </div>
@endif
