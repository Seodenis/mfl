<div class="container digitals margin-top">
    <h2>
        <span>"Мой семейный юрист" в цифрах</span>
    </h2>
    <div class="items flex between wrap align-top center">
        <div class="item width1-3">
            <span class="big"><span class="digital-1">{{ $site->digital_year_days }}</span></span>
            <span class="medium">Дней в году</span>
            <p>мы работаем для вас</p>
        </div>
        <div class="item width1-3">
            <span class="medium">Более</span>
            <span class="big"><span class="digital-2">{{ $site->digital_people }}</span></span>
            <p>человек обратились в наши офисы</p>
        </div>
        <div class="item width1-3">
            <span class="big"><span class="digital-3">{{ $site->digital_percent }}</span>%</span>
            <span class="medium">Клиентов</span>
            <p>получили решение в день обращения</p>
        </div>
        <div class="item width1-1">
            <span class="medium">Всего</span>
            <span class="big">1 шаг</span>
            <span>может решить ваши юридические проблемы</span>
        </div>
    </div>
</div>

@section('javascript')
    <script type="text/javascript">
        $(function () {
            digitalCount('.digital-1', 2000);
            digitalCount('.digital-2', 1900);
            digitalCount('.digital-3', 1650);
        });

        function digitalCount(element, time) {
            var target_block = $(element);
            var target_val = target_block.text();
            var blockStatus = true;
            $(window).scroll(function () {
                var scrollEvent = ($(window).scrollTop() > (target_block.position().top - $(window).height()));
                if (scrollEvent && blockStatus) {
                    blockStatus = false;
                    $({numberValue: 0}).animate({numberValue: target_val}, {
                        duration: time,
                        easing: "linear",
                        step: function (val) {
                            $(element).html(Math.ceil(val));
                        }
                    });
                }
            });
        }
    </script>
@endsection
