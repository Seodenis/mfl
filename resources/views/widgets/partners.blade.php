@if($items)
    <div class="{{ isset($config['notWhitebg']) ? '' : 'whitebg ' }}partners margin-top">
        <div class="container">
            <h2>
                <span>{{ __('Наши партнёры') }}</span>
            </h2>
            <ul class="slider-partners slider-container owl-carousel owl-theme">
                @foreach($items as $k => $v)
                    <li class="item">
                        @if($v->image)
                            <img src="{{ front_storage_path($v->image) }}"
                                alt="{{ $v->title }}">
                        @endif
                        <p class="bottom-text">{{ $v->title }}</p>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
