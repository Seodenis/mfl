<div class="specializations margin-top">
    <div class="container">
        <h2>
            <span>Специализации</span>
        </h2>
    </div>
    <div class="lines">
        <div class="line">
            <div class="container flex between wrap">
                @foreach($items as $key => $value)
                    <div class="item width1-5">
                        <div class="top">
                            <i></i>
                            <a href="{{ $value->uri }}" class="title">{{ $value->short_title }}</a>
                        </div>
                        <ul>
                            @foreach($value->itemsData as $k => $v)
                                <li>
                                    <a href="/{{ $v->uri }}">{{ $v->title }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    @if(($key + 1) % 5 == 0)
                        </div>
                    </div>
                    <div class="line">
                        <div class="container flex between wrap">
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
