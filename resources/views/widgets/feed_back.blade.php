<div class="width1-2 right">
    <div class="contacts">
        {!! Form::open(['route' => 'requests.add', 'class' => 'form-with-types']) !!}
        {!! Form::hidden('save', true) !!}
        {!! Form::hidden('url', url()->current()) !!}
        @if(isset($config['title']))
            <div class="{{ isset($config['textarea']) ? 'title' : 'form-title' }}">{{ $config['title'] }}</div>
        @endif
        @if(isset($config['textarea']))
            <textarea class="textarea-question" placeholder="{{ __('Ваше сообщение') }}" name="message"></textarea>
        @endif
        <div class="flex between c-types">
            <label class="c-item shadow flex align-center center wrap phone checked">
                <input type="radio" name="type" value="PHONE_CALL" checked>
                <i class="fa fa-phone"></i>
                <span class="c-item-text">{{ __('Обратный звонок') }}</span>
                <span class="type">{{ __('обратный звонок') }}</span>
                <span class="placeholder">{{ __('Ваш номер телефона') }}</span>
            </label>
            <label class="c-item shadow flex align-center center wrap whatsapp">
                <input type="radio" name="type" value="VIBER_CALL">
                <i class="fa fa-whatsapp"></i>
                <span class="c-item-text">{{ __('Viber') }}</span>
                <span class="type">{{ __('Viber') }}</span>
                <span class="placeholder">{{ __('Ваш номер Viber') }}</span>
            </label>
            <label class="c-item shadow flex align-center center wrap telegram">
                <input type="radio" name="type" value="TELEGRAM_CALL">
                <i class="fa fa-telegram"></i>
                <span class="c-item-text">{{ __('Telegram') }}</span>
                <span class="type">{{ __('Telegram') }}</span>
                <span class="placeholder">{{ __('Ваш номер Telegram') }}</span>
            </label>
            <label class="c-item shadow flex align-center center wrap whatsapp">
                <input type="radio" name="type" value="WHATS_APP_CALL">
                <i class="fa fa-whatsapp"></i>
                <span class="c-item-text">{{ __('Whats\'app') }}</span>
                <span class="type">{{ __('whats\'app') }}</span>
                <span class="placeholder">{{ __('Ваш номер Whats\'app') }}</span>
            </label>
            <label class="c-item shadow flex align-center center wrap skype">
                <input type="radio" name="type" value="SKYPE_CALL">
                <i class="fa fa-skype"></i>
                <span class="c-item-text">{{ __('Skype') }}</span>
                <span class="type">{{ __('skype') }}</span>
                <span class="placeholder">{{ __('Ваш логин Skype') }}</span>
            </label>
        </div>
        <div class="inputs-btns flex between">
            <div class="input-wrap width1-2 contact">
                <input type="text" name="contact_info" required class="type" placeholder="{{ __('Выберите вид связи') }}">
            </div>
            <div class="input-wrap width1-2 submit">
                <input type="submit" class="btn green" placeholder="{{ __('Свяжитесь со мной') }}">
            </div>
        </div>
        <div class="text">{{ __('Нажимая кнопку "Отправить" вы соглашаетесь с') }}
            <a href='javascript:void(0);'>{{ __('политикой конфиденциальности') }}</a>
        </div>
        {!! Form::close() !!}
    </div>
</div>
