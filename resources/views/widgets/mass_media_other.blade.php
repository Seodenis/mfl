<div class="other-articles">
    <h2>{{ __('Другие публикации') }}</h2>
    <ul>
        @foreach($items as $k => $v)
            <li>
                <div class="date">
                    <i class="fa fa-calendar"></i>
                    <span>{{ $v->created_at->format('d.m.Y') }}</span>
                </div>
                <span>|</span>
                <a href="/{{ $v->uri }}">{{ $v->title }}</a>
            </li>
        @endforeach
    </ul>
</div>
