<div class="slideshow-box">
    <div class="slideshow owl-carousel owl-theme">
        @foreach($items as $k => $v)
            <div
                class="item slide flex align-center"
                style="background:url({{ front_storage_path(bannerImage($v->src)) }}) no-repeat;">
                <div class="container">
                    <h1>{{ $v->title }}</h1>
                    {!! $v->text !!}
                    @if($v->button_url && $v->button_title)
                        <div class="action flex between align-center">
                            {!! $v->button_url !!}{{ $v->button_title }}</a>
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</div>
