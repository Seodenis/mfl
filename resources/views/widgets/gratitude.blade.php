@if($items)
<div class="whitebg testimonials margin-top ">
    <div class="container">
        <h2><span>Что о нас говорят</span></h2>
        <ul class="slider-testimonials slider-container owl-carousel owl-theme">
            @foreach($items as $k => $v)
            <li class="item">
                {!! $v->text !!}
            </li>
            @endforeach
        </ul>
    </div>
</div>
@endif
