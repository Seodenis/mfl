{{--@if($items)--}}
{{--<ul class="inline-block">--}}
{{--@foreach($items as $k => $v)--}}
{{--<li class="d-inline-block col-md-3 bg-warning">--}}
{{--@if ($v->uri)--}}
{{--<a href="{{ $v->uri }}">--}}
{{--@endif--}}
{{--<div>{{ $v->title }}</div>--}}
{{--@if ($v->uri)--}}
{{--</a>--}}
{{--@endif--}}
{{--</li>--}}
{{--@endforeach--}}
{{--</ul>--}}
{{--@endif--}}

<div class="container smi margin-top">
    <h2>
        <span>О нас пишут</span>
    </h2>
    <ul class="slider-smi slider-container owl-carousel owl-theme">
        @foreach($items as $k => $v)
            <li class="item">
                <div class="img-wrap flex align-center center">
                    <img src="{{ front_storage_path($v->image) }}">
                </div>
                <p class="bottom-text">{{ $v->title }}</p>
            </li>
        @endforeach
    </ul>
    <i class="clr"></i>
</div>
