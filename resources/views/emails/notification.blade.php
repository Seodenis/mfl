@extends('layouts.email')

@section( 'page-title', $subject  )

@section('content')
    @if ($user->name)
        <tr class="tr-data">
            <td style="padding: 20px;margin: 0">
                <p style="Margin: 0;-webkit-text-size-adjust: none;-ms-text-size-adjust: none;mso-line-height-rule: exactly;font-size: 14px;font-family: arial, 'helvetica neue', helvetica, sans-serif;line-height: 21px;color: #333333;">Здравствуйте, {{ $user->name }}!</p>
            </td>
        </tr>
    @endif
    @if ($text)
        <tr class="tr-data">
            <td style="padding: 20px;margin: 0">
                <p style="Margin: 0;-webkit-text-size-adjust: none;-ms-text-size-adjust: none;mso-line-height-rule: exactly;font-size: 14px;font-family: arial, 'helvetica neue', helvetica, sans-serif;line-height: 21px;color: #333333;">{!! $text !!}</p>
            </td>
        </tr>
    @endif
@endsection