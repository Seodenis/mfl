@extends('layouts.app')

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('won_cases.index') }}

        <div class="flex between align-center">
            <h1>{{ __('Выигранные дела') }}</h1>
        </div>

        <div class="two-columns flex between align-top">

            <div class="articles">
                @if($items)
                    @foreach($items as $k => $v)

                        <div class="item">
                            <a
                                href="javascript:void(0);" onclick="getWonCasesImages({{ $v->images }});"
                                class="title">{{ $v->title }}</a>
                            <div class="undertitle-article-block flex start align-center">
                                <div class="category flex start align-center">
                                    @if($v->specialization)
                                        <i class="{{ $v->specialization->css_class }}"></i>
                                        <span>{{ $v->specialization->short_title }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="desc text">
                                {!! description($v->description, $v->text, 2000) !!}
                            </div>
                            <div class="btns">
                                <a
                                    class="btn green"
                                    onclick="getWonCasesImages({{ $v->images }});"
                                    href="javascript:void(0);">
                                    Ознакомиться с
                                    делом
                                </a>
                            </div>
                        </div>
                    @endforeach
                    {{ $items->render() }}
            </div>
            @endif
        </div>
    </div>
@endsection
