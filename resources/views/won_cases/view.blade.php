@extends('layouts.app')

@section('meta_title'){{ $item->meta_title }}@endsection
@section('meta_description'){{ $item->meta_description }}@endsection

@section('content')
    <div class="row">
        @if($item->images)
            <div class="col-md-5">
                <img width="100%" src="{{ front_storage_path(json_decode($item->images)[0]) }}">
            </div>
            <div class="col-md-7">
                <h3>{{ $item->title }}</h3>
                @if($item->specialization)
                    <h5>{{ $item->specialization->short_title }}</h5>
                @endif
                <div>{!! $item->text !!}</div>
                <div>{!! $item->bottom_text !!}</div>
            </div>
        @else
            <div class="col-md-12">
                <h3>{{ $item->title }}</h3>
                @if($item->specialization)
                    <h5>{{ $item->specialization->short_title }}</h5>
                @endif
                <div>{!! $item->text !!}</div>
                <div>{!! $item->bottom_text !!}</div>
            </div>
        @endif
    </div>
@endsection