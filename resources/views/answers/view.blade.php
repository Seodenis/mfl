@extends('layouts.app')

@section('meta_title'){{ $item->meta_title }}@endsection
@section('meta_description'){{ $item->meta_description }}@endsection

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('answers.item', $item) }}
        <div class="question-answer-article">
            <h1>{{ $item->title }}</h1>

            <div class="text">
                <div class="question-block">
                    <h2>{{ _('Вопрос') }}</h2>
                    {!! $item->question !!}
                </div>
                <div class="answer-block">
                    <h2>{{ _('Ответ') }}</h2>
                    {!! $item->answer !!}
                </div>
                @if($item->bottom_text)
                    <div>{!! $item->bottom_text !!}</div>
                @endif
            </div>

            @widget('AnswersOther', ['notId' => $item->id])

            <div class="subscribe contacts in-article">
                <h2>Хотите получать бесплатные юридические советы, новости, скидки и информацию об акциях?</h2>
                <form class="flex between align-center">
                    <div class="input-wrap width1-2 contact">
                        <input type="text" class="email" placeholder="Ваш e-mail">
                    </div>
                    <div class="input-wrap width1-2 submit">
                        <input type="submit" class="btn green" value="Подписаться">
                    </div>
                </form>
                <p class="policy">*нажимая на кнопку "Подписаться" вы соглашаетесь с
                    <a href="javascript:void(0);">правилами</a>
                    на получение рассылки
                </p>
            </div>
        </div>
    </div>
@endsection
