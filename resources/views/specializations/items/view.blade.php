@extends('layouts.app')

@section('meta_title'){{ $item->meta_title }}@endsection
@section('meta_description'){{ $item->meta_description }}@endsection

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('specializations.items.item', $item) }}
        <h1>{{ $item->title }}</h1>

        {{--<div class="flex between align-top">
            <div>{!! $item->text !!}</div>
        </div>--}}

        <div class="flex between align-top">
            <div class="width1-3 photo">
                <img
                    class="shadow"
                    src="{{ $item->intro_image ? front_storage_path($item->intro_image) : '/images/team/kashleva-big.jpg' }}"
                    alt=''>
                <p class="photo-undertitle">{{ $item->intro_image_title ? $item->intro_image_title : 'Дарья Кашлева / юрист по семейным делам' }}</p>
            </div>
            <div class="we-help width2-3 green-titles">
                @if($item->intro_text)
                    {!! $item->intro_text !!}
                @else
                    <h2>Мы поможем вам:</h2>
                    <ul>
                        <li>Проконсультируем по вопросу просто и понятно. Подготовим любые необходимые документы.</li>
                        <li>Проконсультируем по вопросу просто и понятно.</li>
                        <li>Подготовим любые необходимые документы.</li>
                    </ul>
                @endif
            </div>
        </div>

        @include('partials.write-questions')

        {{--<div class="specialisation-articles margin-top">
            <h2><span>Мы готовы помочь вам по следующим вопросам</span></h2>
            <div class="flex between align-top">
                @if($item->category->itemsFullData)
                    @php ($halfCount = round($item->category->itemsFullData->count() / 2))
                    <ul class="width1-2">
                        @foreach($item->category->itemsFullData as $k => $v)
                            <li>
                                <a href="/{{ $v->uri }}">{{ $v->title }}</a>
                            </li>
                            @if(($k + 1) % $halfCount == 0)
                                @php ($halfCount = 1000)
                    </ul>
                    <ul class="width1-2">
                        @endif
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>--}}

        @widget('WonCases')

        <div class="text margin-top">{!! $item->text !!}</div>
    </div>
@endsection
