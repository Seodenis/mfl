<ul class="quicknav flex start wrap">
    @foreach($items as $k => $v)
        <li>
            <a href="/{{ $v->uri }}">{{ $v->short_title }}</a>
        </li>
    @endforeach
</ul>
