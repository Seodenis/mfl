@extends('layouts.app')

@section('meta_title'){{ $item->meta_title }}@endsection
@section('meta_description'){{ $item->meta_description }}@endsection

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('specializations.item', $item) }}

        <h1>{{ $item->title }}</h1>
        <div class="flex between align-center">
            <div class="text">
                <div>{!! $item->text !!}</div>
            </div>
        </div>

        @include('partials.write-questions')

        <div id="specialisation-articles" class="specialisation-articles margin-top">
            <h2><span>Мы готовы помочь вам по следующим вопросам</span></h2>
            <div class="flex between align-top">
                @if($dataItems)
                    @php ($halfCount = round($dataItems->count() / 2))
                    <ul class="width1-2">
                        @foreach($dataItems as $k => $v)
                            <li>
                                <a href="/{{ $v->uri }}">{{ $v->title }}</a>
                            </li>
                            @if(($k + 1) % $halfCount == 0)
                            @php ($halfCount = 1000)
                            </ul>
                                <ul class="width1-2">
                            @endif
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>

        @widget('WonCases')

        <div class="text margin-top">{!! $item->bottom_text !!}</div>

    </div>
@endsection
