@extends('layouts.app')

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('specializations.index') }}

        <h1>{{ __('Специализации') }}</h1>

        @include('specializations.partials.quicknavigation')

        @if($items)
            <div class="specializations-items items flex between wrap">
                @foreach($items as $key => $value)
                    <div class="item width1-2 whitebg padding30-20" id="{{ $value->slug }}">
                        <div class="top flex start align-center">
                            <i class="{{ $value->css_class }}"></i>
                            <div class="title">{{ $value->short_title }}</div>
                        </div>
                        <ul>
                            @foreach($value->itemsData as $k => $v)
                                <li>
                                    <a href="/{{ $v->uri }}">{{ $v->title }}</a>
                                </li>
                            @endforeach
                        </ul>
                        <a class="show-all-services" href="{{ $value->uri }}#specialisation-articles">{{ __('Показать все услуги') }}</a>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
@endsection
