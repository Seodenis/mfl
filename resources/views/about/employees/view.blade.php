@extends('layouts.app')

@section('meta_title'){{ $item->meta_title }}@endsection
@section('meta_description'){{ $item->meta_description }}@endsection

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('about.employees.item', $item) }}

        <div class="person-single-top flex between align-center">
            <div class="about width2-3">
                <h1>{{ $item->title }}</h1>
                @if($item->position)
                    <p class="role">{{ $item->position->title }}</p>
                @endif
                <div class="about">
                    {!! $item->text !!}
                </div>
            </div>
            @if($item->image)
                <div class="width1-3 photo">
                    <img class="shadow" src="{{ front_storage_path($item->image) }}" alt="{{ $item->title }}">
                </div>
            @endif
        </div>

        <div class="person-single-text text margin-top">
            {!! $item->bottom_text !!}
        </div>

        @widget('WonCases')

    </div>
@endsection
