@extends('layouts.app')

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('about.employees.index') }}

        <h1>Заключение брачного договора между супругами</h1>
        <p>Мы сплоченная команда профессионалов и энтузиастов своего дела, горячо болеющих за результат каждого
            клиента.
        </p>

        @if($items)
            <div class="team flex center align-top wrap margin-top">
                @foreach($items as $k => $v)
                    <div class="item width1-3">
                        @if($v->image)
                            <div class="image">
                                <a href="/{{ $v->uri }}">
                                    <img src="{{ front_storage_path($v->image) }}">
                                </a>
                            </div>
                        @endif
                        <div class="name">
                            <a href="/{{ $v->uri }}">{{ $v->title }}</a>
                        </div>
                        @if($v->position)
                            <div class="role">{{ $v->position->title }}</div>
                        @endif
                    </div>
                @endforeach
                {{ $items->render() }}
            </div>
    </div>
    @endif
@endsection
