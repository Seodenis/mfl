@extends('layouts.app')

@section('content')
    @if($items)
        <ul class="inline-block">
            @foreach($items as $k => $v)
                <li>
                    <h3>{{ $v->title }}</h3>
                    @if($v->mass_media)
                        @if($v->mass_media->image)
                            <img src="{{ front_storage_path($v->mass_media->image) }}">
                        @endif
                        <span>{{ $v->mass_media->title }}</span>
                    @endif
                    <div>{!! description($v->description, $v->text) !!}</div>
                    <a href="/{{ $v->uri }}">{{ _('Подробнее') }}</a>
                </li>
            @endforeach
        </ul>

        {{ $items->render() }}
    @endif
@endsection
