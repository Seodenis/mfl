<div class="mainpage">
    <div class="mod_bg flex center align_center">
        <div class="mod_body white">
            <div class="form">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <ul class="formErrors"></ul>
                    <div>
                        <label for="email">Ваш e-mail</label>
                        <input id="email" type="email"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="submit">
                        <input class="btn green" type="submit" value="{{ __('Восстановить') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
