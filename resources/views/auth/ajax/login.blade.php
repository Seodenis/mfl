<div id="tabs">
    <ul class="tabs flex between wrap">
        <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active">
            <a href="{{ route('login') }}" class="popup-ajax-link ui-tabs-anchor">Войти</a>
        </li>
        <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab">
            <a href="{{ route('register') }}" class="popup-ajax-link ui-tabs-anchor">Регистрация</a>
        </li>
    </ul>
    <div id="tabs-1" class="tab_space form">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <ul class="formErrors"></ul>
            <div>
                <label for="email">{{ __('Ваш e-mail или номер телефона') }}</label>
                <input
                    id="email" type="email"
                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                    value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
            <div>
                <label>{{ __('Пароль') }}</label>
                <input
                    id="password" type="password"
                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                    name="password"
                    required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>

            <label style="position: absolute;">
                <input
                    type="checkbox"
                    name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Запомнить меня') }}
            </label>

            <div class="forget_password">
                <a
                    class="popup-ajax-link"
                    href='{{ route('password.request') }}'>{{ __('Забыли пароль?') }}</a>
            </div>
            <div class="submit">
                <input type="submit" class="btn green" value="{{ __('Войти') }}">
            </div>
        </form>
    </div>
</div>
