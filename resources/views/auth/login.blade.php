@extends('layouts.app')

@section('content')
    <div class="mainpage" style="margin: 140px 0 0 0">
        <div class="flex center align_center">
            <div class="mod_body">
                <h1>Уважаемый пользователь!</h1>
                <p>{{ _('Пожалуйста, пройдите авторизацию для получения доступа к полному функционалу сервиса. Спасибо!') }}</p>
                <div id="tabs">
                    <div id="tabs-1" class="tab_space form">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <ul class="formErrors" style="padding: 0;border:none"></ul>
                            <div>
                                <label for="email">{{ __('Ваш e-mail или номер телефона') }}</label>
                                <input id="email" type="email"
                                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                       value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div>
                                <label>{{ __('Пароль') }}</label>
                                <input id="password" type="password"
                                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password"
                                       required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <label style="position: absolute;">
                                <input type="checkbox"
                                       name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Запомнить меня') }}
                            </label>

                            <div class="forget_password"><a class="popup-ajax-link"
                                                            href='{{ route('password.request') }}'>{{ __('Забыли пароль?') }}</a>
                            </div>
                            <div class="submit">
                                <input type="submit" class="btn green" value="{{ __('Войти') }}">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
