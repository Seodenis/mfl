@extends('layouts.app')

@section('content')
    <div class="mainpage" style="margin: 140px 0 0 0;">
        <div class="flex center align_center">
            <div class="mod_body">
                <h1>Регистрация</h1>
                <div id="tabs">
                    <div id="tabs-2" class="tab_space form">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <ul class="formErrors" style="padding: 0;border: 0"></ul>
                            <div>
                                <label for="name">{{ __('Имя') }}</label>
                                <input
                                    id="name" type="text"
                                    class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                    name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div>
                                <label for="email">{{ __('Email') }}</label>
                                <input
                                    id="email" type="email"
                                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                    value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div>
                                <label for="phone">Номер телефона</label>
                                <input
                                    id="phone" type="tel"
                                    class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                    name="phone"
                                    required>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div>
                                <label for="password">{{ __('Пароль') }}</label>
                                <input
                                    id="password" type="password"
                                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                    name="password"
                                    required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div>
                                <label for="password-confirm">{{ __('Повторите пароль') }}</label>
                                <input
                                    id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation"
                                    required>
                            </div>
                            <div class="checkbox">
                                <label id="service-terms" style="font-size: 12px">
                                    <input type="checkbox" name="">
                                    <span>{{ __('Я принимаю условия') }}</span>
                                    <span><a
                                            href="{{ route('single-main-article', ['slug' => 'service-terms']) }}"
                                            target="_blank">{{ __('пользовательского соглашения') }}</a></span>
                                    <span style="display: block;line-height: 22px;margin: -10px 0 10px 0">{{ __('и даю согласие на обработку моих') }}
                        <a
                            href="{{ route('single-main-article', ['slug' => 'personal-data']) }}"
                            target="_blank">{{ __('персональных данных') }}</a>
                    </span>
                                </label>
                            </div>
                            <div class="submit">
                                <div
                                    class="error-info error">{{ __('Для использования сервиса необходимо принять условия!') }}</div>
                                <input disabled type="submit" class="btn green" value="{{ __('Зарегистрироваться') }}">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('innerScripts')
    <script type="text/javascript">

        var serviceTerms = $('#service-terms input[type=checkbox]');
        var registerSubmit = $('.submit input[type=submit]');

        $(serviceTerms).on('click', function () {

            if (serviceTerms.prop("checked")) {
                registerSubmit.removeAttr('disabled');
                $('.error-info.error').fadeOut();
            } else {
                registerSubmit.attr('disabled', 'disabled');
                $('.error-info.error').fadeIn();
            }
        });

        $(registerSubmit).on('click', function () {
            if (!serviceTerms.prop("checked")) {
                $('.error-info.error').fadeIn();
            }
        });

    </script>
@endsection
