@extends('layouts.app')

@section('content')
    <div class="mainpage" style="margin: 20px 0 0 0">
        <div class="flex center align_center">
            <div class="mod_body white">
                <h2>{{ __('Восстановление пароля') }}</h2>
                <div class="form">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <ul class="formErrors"></ul>
                        <div>
                            <label for="email">Ваш e-mail</label>
                            <input id="email" type="email"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                   value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="submit">
                            <input class="btn green" type="submit" value="{{ __('Восстановить') }}">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
