@extends('layouts.app')

@section('content')

    <div class="content">

        {{ Breadcrumbs::render('profile.settings') }}

        <h1>{{ __('Настройки') }}</h1>
        <div class="profile flex between align_top">
            {!! Form::open(['route' => 'profile.settings', 'class' => 'half']) !!}
            {{ Form::hidden('save', true) }}
            <div class="profile_input">
                {{ Form::label('name', 'Ваша ФИО') }}
                {{ Form::text('name', $user->name, ['placeholder' => 'Представьтесь, пожалуйста', 'required' => true]) }}
                <span class="edit"></span>
            </div>
            <div class="profile_input">
                {{ Form::label('phone', 'Ваш номер телефона') }}
                {{ Form::text('phone', $user->phone, ['required' => true]) }}
                <span class="edit"></span>
            </div>
            <div class="profile_input">
                {{ Form::label('email', 'Ваш e-mail') }}
                {{ Form::text('email', $user->email, ['required' => true]) }}
                <span class="edit"></span>
            </div>
            <div class="profile_input ">
                {{ Form::label('birthdate', 'Дата рождения') }}
                {{ Form::text('birthdate', dateFormat($user->birthdate), ['class' => 'datepicker']) }}
                <span class="edit"></span>
            </div>
            <div class="profile_input field">
                {{ Form::label('city', 'Город проживания') }}
                {{ Form::text('city', $user->city) }}
                <span class="edit"></span>
            </div>
            <div style="text-align:center;">
                {{ Form::submit('Сохранить изменения', ['class' => 'btn green']) }}
            </div>
            {!! Form::close() !!}
            <div class="half">
                <h2>Смена пароля</h2>
                {!! Form::open(['route' => 'profile.settings', 'class' => 'password_widget']) !!}
                {{ Form::hidden('save', true) }}
                <div class="pass_input">
                    {{ Form::label('password', 'Новый пароль') }}
                    {{ Form::password('password', ['required' => true, 'minlength' => 5]) }}
                    <span class="show"></span>
                </div>
                <div class="pass_input">
                    {{ Form::label('re_password', 'Подтвердите пароль') }}
                    {{ Form::password('re_password', ['required' => true, 'minlength' => 5]) }}
                    <span class="show"></span>
                </div>
                <div style="text-align:center;">
                    <input type="submit" class="btn green" value="Сменить пароль">
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
