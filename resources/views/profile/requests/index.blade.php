@extends('layouts.app')

@section('content')
    <div class="content">
        {{ Breadcrumbs::render('profile.requests') }}

        <h1>{{ __('Заявки') }}</h1>

        <div class="scrollable-table">
            <table class="applications">
                <tr>
                    <th>{{ __('Дата заявки') }}</th>
                    <th>{{ __('Номер заявки') }}</th>
                    <th>{{ __('Статус') }}</th>
                    <th></th>
                </tr>
                @foreach($items as $k => $v)
                    <tr>
                        <td>{{ $v->created_at->format('d.m.Y') }}</td>
                        <td>{{ $v->id }}</td>
                        <td>{{ $v->status->title }}</td>
                        <td>
                            <a class="btn green" href="{{ route('profile.requests.item', ['id' => $v->id]) }}">Открыть</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
