@extends('layouts.app')

@section('title')Обращение@endsection

@section('content')
    <div class="content">
        {{ Breadcrumbs::render('profile.requests.item', $item) }}

        <h1>{{ __('Заявка') }} {{ $item->id }}</h1>
        <div class="scrollable-table">
            <table class="applications">
                <tr>
                    <th>{{ __('Дата заявки') }}</th>
                    <th>{{ __('Номер заявки') }}</th>
                    <th>{{ __('Статус') }}</th>
                </tr>
                <tr>
                    <td>{{ $item->created_at->format('d.m.Y') }}</td>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->status->title }}</td>
                </tr>
            </table>
        </div>
        <div id="app" data-requestId="{{ $item->id }}">
            <chat-component></chat-component>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        function toPay(sum, orderId) {
            $.redirectPost("{{ env('PAYKEEPER_URL') }}/create", {sum: sum, orderid: orderId, clientid: "{{ auth()->user()->name }}", client_email: "{{ auth()->user()->email }}"});
        }
    </script>
@endsection
