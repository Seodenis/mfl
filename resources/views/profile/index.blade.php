@extends('layouts.app')

@section('content')
    <div class="wrapper_flex flex between align_top">
        <div class="pseudo_left_panel_box"></div>
        <div class="content">
            <div class="container">
                <h1>{{ _('Личный кабинет') }}</h1>
            </div>
        </div>
    </div>
@endsection
