@extends('layouts.app')

@section('content')
    <div class="content">
        {{ Breadcrumbs::render('partners.index') }}
        @widget('Partners', ['notWhitebg' => true])
        @widget('MassMedia')
        @widget('DigitalInfo')
    </div>
@endsection
