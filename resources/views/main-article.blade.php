@extends('layouts.app')

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('article.data', 'work', $item) }}

        <h1>{{ $item->title }}</h1>
        <div class="text">
            <div>{!! $item->text !!}</div>
        </div>
    </div>
@endsection
