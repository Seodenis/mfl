@extends('layouts.app')

@section('content')
    <div class="wrapper_flex flex between align_top">
        <div class="pseudo_left_panel_box"></div>
        <div class="content">
            <div class="container">
                <div class="profile flex between align_top">
                    {!! Form::open(['route' => 'requests.add']) !!}
                    {{ Form::hidden('save', true) }}
                    {{ Form::hidden('url', url()->previous()) }}
                    <div class="profile_input">
                        {{ Form::label('contact_info', 'Ваш номер телефона') }}
                        {{ Form::text('contact_info', false, ['required' => true]) }}
                    </div>
                    <div class="profile_input">
                        {{ Form::label('message', 'Сообщение') }}
                        {{ Form::textarea('message') }}
                    </div>
                    <div style="text-align:center;">
                        {{ Form::submit('Отправить', ['class' => 'green btn']) }}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
