@extends('layouts.app')

@section('meta_title'){{ $item->meta_title }}@endsection
@section('meta_description'){{ $item->meta_description }}@endsection

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('article.item', $item->category, $item) }}

        <h1>{{ $item->title }}</h1>
        <div class="date">
            <i class="fa fa-calendar"></i>
            <span>{{ $item->created_at->format('d.m.Y') }}</span>
        </div>

        <div class="text">
            {!! $item->text !!}
            {!! $item->bottom_text !!}
        </div>
    </div>
@endsection
