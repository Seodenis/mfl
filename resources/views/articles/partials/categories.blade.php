@if($categories)
        <aside>
            <ul class="menu shadow">
                @foreach($categories as $k => $v)
                    <li{{ request('catSlug') == $v->slug ? ' class=active' : false }}>
                        <a href="/{{ $v->uri }}">{{ $v->title }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="subscribe contacts shadow">
                <p>Хотите получать бесплатные юридические советы, новости, скидки и информацию об акциях?</p>
                <form>
                    <div class="input-wrap width1-1 contact">
                        <input type="text" class="email" placeholder="Ваш e-mail">
                    </div>
                    <div class="input-wrap width1-1 submit">
                        <input type="submit" class="btn green" value="Подписаться">
                    </div>
                </form>
                <p class="policy">*нажимая на кнопку "Подписаться" вы соглашаетесь с
                    <a href="javascript:void(0);">правилами</a>
                    на получение рассылки
                </p>
            </div>
        </aside>
@endif
