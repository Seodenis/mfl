@extends('layouts.app')

@section('meta_title'){{ $category->meta_title }}@endsection
@section('meta_description'){{ $category->meta_description }}@endsection

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('article.category', $category) }}

        <h1>{{ $category->title }}</h1>

        <div class="two-columns flex between align-top">

            @include('articles.partials.categories')

            @if($items)
                <div class="articles">
                    @foreach($items as $k => $v)
                        <div class="item">
                            <a href="/{{ $v->uri }}" class="title">{{ $v->title }}</a>
                            <div class="undertitle-article-block flex start align-center">
                                <div class="date">
                                    <i class="fa fa-calendar"></i>
                                    <span>{{ $v->created_at->format('d.m.Y') }}</span>
                                </div>
                            </div>
                            <div class="desc">{!! description($v->description, $v->text) !!}</div>
                            <div class="btns">
                                <a class="btn green" href="/{{ $v->uri }}">Читать далее</a>
                            </div>
                        </div>
                    @endforeach
                    {{ $items->render() }}
                </div>
            @endif
        </div>
    </div>
@endsection
