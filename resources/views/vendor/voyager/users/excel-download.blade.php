<table>
    <thead>
    <tr>
        <th>{{ _('Имя') }}</th>
        <th>{{ _('E-mail') }}</th>
        <th>{{ _('Телефон') }}</th>
        <th>{{ _('День рождения') }}</th>
        <th>{{ _('Город') }}</th>
    </tr>
    </thead>
    <tbody>
    @include('voyager::users.partials.excel-download-items')
    </tbody>
</table>