@foreach ($items as $k => $v)
    <tr>
        <td style="text-align: left">{{ $v->name }}</td>
        <td style="text-align: left">{{ $v->email }}</td>
        <td style="text-align: left">{{ $v->phone }}</td>
        <td>{{ $v->birthdate ? dateFormat($v->birthdate, 'd.m.Y') : false }}</td>
        <td>{{ $v->city }}</td>
    </tr>
@endforeach