@extends('layouts.app')

@section('meta_title'){{ $items->first()->meta_title }}@endsection
@section('meta_description'){{ $items->first()->meta_description }}@endsection

@section('content')
    @if($items)
        <ul class="inline-block">
            @foreach($items as $k => $v)
                <li>
                    <h3>{{ $v->title }}</h3>
                    <div>{!! $v->text !!}</div>
                </li>
            @endforeach
        </ul>
    @endif
@endsection
