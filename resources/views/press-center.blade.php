@extends('layouts.app')

@section('meta_title'){{ $items->first()->meta_title }}@endsection
@section('meta_description'){{ $items->first()->meta_description }}@endsection

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('article.data', 'press-center', $items->first()) }}

        <h1>{{ __('Пресс центр') }}</h1>

        <div class="text green-titles">
            @if($items)
                @foreach($items as $k => $v)
                    <div>{!! $v->text !!}</div>
                @endforeach
            @endif

            <div class="press-center flex between align-top">
                <div class="width1-2">
                    <h3>Контакты пресс-службы</h3>
                    <p>
                        <a href="tel:{{ $site->press_service_phone }}">{{ $site->press_service_phone }}</a>
                    </p>
                    <h3>Мы в соцсети</h3>
                    <p>
                        <a target="_blank" href="https://www.instagram.com/moj.yurist">
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a target="_blank" href="http://www.youtube.com/c/МойСемейныйЮристМосква">
                            <i class="fa fa-youtube"></i>
                        </a>
                        <a target="_blank" href="http://vk.com/moj.yurist">
                            <i class="fa fa-vk"></i>
                        </a>
                        <a target="_blank" href="https://telegram.me/myfamilylawyer">
                            <i class="fa fa-telegram"></i>
                        </a>
                        <a target="_blank" href="https://www.facebook.com/moj.yurist/">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                    </p>
                    <h3>{{ __('Логотип для скачивания') }}</h3>
                    <a class="logo-download" download href="{{ asset('/images/logo.png') }}">
                        <img src="{{ asset('/images/logo.png') }}" alt="">
                    </a>
                </div>
                @if($massMedia)
                    <div class="width1-2">
                        <h3>{{ __('Мы в СМИ') }}</h3>
                        <ul>
                            @foreach($massMedia as $k => $v)
                                <li>
                                    <a href="/{{ $v->uri }}">{{ $v->title }}</a>
                                    <div class="undertitle-article-block flex start align-center">
                                        <div class="date">
                                            <i class="fa fa-calendar"></i>
                                            <span>{{ $v->created_at->format('d.m.Y') }}</span>
                                        </div>
                                        <img style="margin: 0 0 0 10px" src="{{ front_storage_path($v->mass_media->image) }}">
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
