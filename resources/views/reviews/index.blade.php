@extends('layouts.app')

@section('content')
    <div class="content">

        {{ Breadcrumbs::render('reviews.index') }}

        <h1>{{ __('Отзывы') }}</h1>
        @if($items)

            <div class="two-columns flex between align-top">

                <div class="articles">
                    @foreach($items as $k => $v)
                        <div class="item">
                            <a href="/{{ $v->uri }}" class="title">{{ $v->title }}</a>
                            <div class="undertitle-article-block flex start align-center">
                                @if($v->mass_media)
                                    @if($v->mass_media->image)
                                        <img src="{{ front_storage_path($v->mass_media->image) }}">
                                    @endif
                                @endif
                                <div class="date">
                                    <i class="fa fa-calendar"></i>
                                    <span>{{ $v->created_at->format('d.m.Y') }}</span>
                                </div>
                            </div>
                            <div class="desc">{!! description($v->description, $v->text) !!}</div>
                            <div class="btns">
                                <a class="btn green" href="/{{ $v->uri }}">{{ _('Подробнее') }}</a>
                            </div>
                        </div>
                    @endforeach

                    {{ $items->render() }}
                </div>
            </div>
        @endif
    </div>
@endsection
