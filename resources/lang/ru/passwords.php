<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен иметь не менее 6 символов.',
    'reset' => 'Ваш пароль сброшен!',
    'sent' => 'Мы отправили Вам ссылку на сброс пароля!',
    'token' => 'Токен сброса пароля не действительный.',
    'user' => "Пользователя с таким email не существует.",

];
