<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute должно быть принято.',
    'active_url'           => ':attribute не валидный URL.',
    'after'                => ':attribute должен быть датой после :date.',
    'after_or_equal'       => ':attribute должен быть датой больше или равно :date.',
    'alpha'                => ':attribute может содержать только буквы.',
    'alpha_dash'           => ':attribute может содержать только буквы, цифры и тире.',
    'alpha_num'            => ':attribute может содержать только буквы и цифры.',
    'array'                => ':attribute должен быть массивом.',
    'before'               => ':attribute должен быть датой меньше :date.',
    'before_or_equal'      => ':attribute должен быть датой меньше или равно :date.',
    'between'              => [
        'numeric' => ':attribute должен быть между :min и :max.',
        'file'    => ':attribute должен быть между :min и :max килобайт.',
        'string'  => ':attribute должен быть между :min и :max символов.',
        'array'   => ':attribute должен быть между :min and :max элементов.',
    ],
    'boolean'              => ':attribute должен быть true или false.',
    'confirmed'            => ':attribute подтверждение не соответствует.',
    'date'                 => ':attribute не валидная дата.',
    'date_format'          => ':attribute не соответствует формату :format.',
    'different'            => ':attribute и :other должны быть разными.',
    'digits'               => ':attribute должны быть :digits цифры.',
    'digits_between'       => ':attribute должен быть между :min и :max цифрами.',
    'dimensions'           => ':attribute не валидное изображение.',
    'distinct'             => ':attribute дубликат.',
    'email'                => ':attribute не валидный email.',
    'exists'               => 'выбранный :attribute не валиден.',
    'file'                 => ':attribute должен быть файл.',
    'filled'               => ':attribute поле должно иметь значение.',
    'gt'                   => [
        'numeric' => ':attribute должен быть больше, чем :value.',
        'file'    => ':attribute должен быть больше, чем :value килобайт.',
        'string'  => ':attribute должен быть больше, чем :value символов.',
        'array'   => ':attribute должен иметь больше, чем :value элементов.',
    ],
    'gte'                  => [
        'numeric' => ':attribute должен быть больше или равен :value.',
        'file'    => ':attribute должен быть больше или равенl :value килобайт.',
        'string'  => ':attribute должен быть больше или равен :value символов.',
        'array'   => ':attribute должен иметь :value элементов или больше.',
    ],
    'image'                => ':attribute должен быть изображением.',
    'in'                   => 'выбранный :attribute не валиден.',
    'in_array'             => ':attribute поле не существует в :other.',
    'integer'              => ':attribute должен быть числом.',
    'ip'                   => ':attribute должен быть валидным IP адресом.',
    'ipv4'                 => ':attribute должен быть валидным IPv4 адресом.',
    'ipv6'                 => ':attribute должен быть валидным IPv6 адресом.',
    'json'                 => ':attribute должен быть валидной JSON строкой.',
    'lt'                   => [
        'numeric' => ':attribute должен быть меньше, чем :value.',
        'file'    => ':attribute должен быть меньше, чем :value килобайт.',
        'string'  => ':attribute должен быть меньше, чем :value символов.',
        'array'   => ':attribute должен иметь меньше, чем :value элементов.',
    ],
    'lte'                  => [
        'numeric' => ':attribute должен быть меньше или равно, чем :value.',
        'file'    => ':attribute должен быть меньше или равно, чем :value килобайт.',
        'string'  => ':attribute должен быть меньше или равно, чем :value символов.',
        'array'   => ':attribute должен быть больше, чем :value элементов.',
    ],
    'max'                  => [
        'numeric' => ':attribute может быть не больше, чем :max.',
        'file'    => ':attribute может быть не больше, чем :max килобайт.',
        'string'  => ':attribute может быть не больше, чем :max символов.',
        'array'   => ':attribute не должен быть больше, чем :max элемнтов.',
    ],
    'mimes'                => ':attribute должен быть файлом типа: :values.',
    'mimetypes'            => ':attribute должен быть файлом типа: :values.',
    'min'                  => [
        'numeric' => ':attribute должен быть не менее :min.',
        'file'    => ':attribute должен быть не менее :min килобайт.',
        'string'  => ':attribute должен быть не менее :min символов.',
        'array'   => ':attribute должен иметь не менее :min элементов.',
    ],
    'not_in'               => 'выбранный :attribute не валиден.',
    'not_regex'            => ':attribute формат не валиден.',
    'numeric'              => ':attribute должен быть числом.',
    'present'              => ':attribute поле должно быть настоящим.',
    'regex'                => ':attribute формат не валиден.',
    'required'             => ':attribute поле обязательно для заполнения.',
    'required_if'          => ':attribute поле обязательно для заполнения, если :other равно :value.',
    'required_unless'      => ':attribute поле обязательно для заполнения, если :other в :values.',
    'required_with'        => ':attribute поле обязательно для заполнения, когда :values настоящее.',
    'required_with_all'    => ':attribute поле обязательно для заполнения, когда :values настоящее.',
    'required_without'     => ':attribute поле обязательно для заполнения, когда :values не настоящее.',
    'required_without_all' => ':attribute поле обязательно для заполнения, когда :values не настоящее.',
    'same'                 => ':attribute и :other должны соответствовать.',
    'size'                 => [
        'numeric' => ':attribute должен быть :size.',
        'file'    => ':attribute должен быть :size килобайт.',
        'string'  => ':attribute должен быть :size символов.',
        'array'   => ':attribute должен содержать :size элементов.',
    ],
    'string'               => ':attribute должен быть строкой.',
    'timezone'             => ':attribute должен быть в валидной зоне.',
    'unique'               => ':attribute уже занят.',
    'uploaded'             => ':attribute загрузка не удалась.',
    'url'                  => ':attribute формат не валидный.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
