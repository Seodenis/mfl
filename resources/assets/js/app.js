require('./bootstrap');
import ElMoment from 'el-moment';

window.Vue = require('vue');

Vue.config.productionTip = false;
Vue.config.devtools = false;
Vue.config.debug = false;
Vue.config.silent = true;

Vue.component(ElMoment.name, ElMoment);
Vue.component('chat-component', require('./components/ChatComponent.vue'));
Vue.component('chat-message-component', require('./components/ChatMessageComponent.vue'));
Vue.component('chat-form-component', require('./components/ChatFormComponent.vue'));
Vue.component('vote-form-component', require('./components/VoteFormComponent.vue'));
Vue.component('chat-request-form-component', require('./components/lawyer/ChatRequestFormComponent.vue'));
Vue.component('chat-requests-form-component', require('./components/lawyer/ChatRequestsFormComponent.vue'));
Vue.component('message-component', require('./components/MessageComponent.vue'));
Vue.component('message-count-component', require('./components/CountMessageComponent.vue'));
Vue.component('alert-count-component', require('./components/CountAlertComponent.vue'));
Vue.component('message-count-lawyer-component', require('./components/CountLawyerMessageComponent.vue'));

Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('DD.MM.YYYY в hh:mm:ss')
    }
});

const app = new Vue({
    el: '#app'
});
