$(document).ready(function () {
    init();
});

function init() {
    /*$('input[name="city"]').kladr({
        type: $.kladr.type.city
    });*/
    getPopup('.popup-ajax-link');

    $(".datepicker").datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: '-90:+0',
        maxDate: 0
    });

    $.datepicker.regional["ru"];

    $('input[name="city"]').kladr({
        type: $.kladr.type.city
    });
    $(".success, .exception, .alert").dialog({
        modal: true,
        title: false,
        width: 'auto',
        height: 'auto',
        dialogClass: 'dialogStyle',
        buttons: true
    }).dialog("widget").find(".ui-dialog-title").hide();

    $(document).on('click', '.ui-widget-overlay', function () {
        $(".ui-dialog-titlebar-close").trigger('click');
    });
    $('#preloader').fadeOut();
}

// Send form with post data
$.extend(
    {
        redirectPost: function (location, args) {
            var form = '';
            $.each( args, function( key, value ) {
                form += '<input type="hidden" name="'+key+'" value="'+value+'">';
            });
            $('<form action="'+location+'" method="POST">'+form+'</form>').appendTo('body').submit();
        }
    }
);

$('a[href^="#"]').on('click', function () {
    var target = $(this).attr('href');
    var headerHeight = $('header').height();
    $('html, body').animate({scrollTop: $(target).offset().top - headerHeight}, 800);
    return false;
});

/**
 * Вызов окна с данными
 * @author ansotov
 * @param attr
 */
function getPopup(attr) {

    $(attr).on('click', function (event) {

        var redirectUrl = ($(this).data('href')) ? $(this).data('href') : false;

        // ansotov останавливаем переход по ссылке
        event.preventDefault();

        window.stop();

        $.ajax({
            url: this.href,
            success: function (data) {

                $.fancybox(data, {
                    autoSize: true
                });

                // Reinit inner links
                getPopup('.fancybox-inner .popup-ajax-link');

                //ansotov отправка формы без перезагрузки
                sendForm('.fancybox-inner form', redirectUrl);
            },
            error: function (request, status, error) {
                window.location.reload();
            }
        });
    });
}

/**
 * Отправка формы без перезагрузки
 * @author ansotov
 * @param attr
 */
function sendForm(attr, redirectUrl) {
    $(attr).on('submit', function (event) {

        // ansotov останавливаем переход по ссылке
        event.preventDefault();

        //ansotov получаем url
        var href = $(this).attr('action');

        //ansotov формируем данные из формы
        var formData = $(this).serializeToObject();

        $.ajax({
            url: href,
            method: 'POST',
            data: formData,
            success: function (data) {

                if (data == 'error') {
                    var backData = '<div class="mod_body">\n' +
                        '            <h2>Уважаемый пользователь!</h2>\n' +
                        '            <p>Пожалуйста, авторизуйтесь и пополните баланс в личном кабинете для получения полного функционала и оплаты\n' +
                        '                создания документа. Спасибо!</p>\n' +
                        '            <div id="tabs" style="text-align: center">\n' +
                        '                <div id="tabs-1" class="tab_space form">\n' +
                        '                    Логин или пароль не верны, попробуйте снова.' +
                        '                        <a href="/login" class="submit popup-ajax-link">\n' +
                        '                            <input type="submit" value="Войти">\n' +
                        '                        </a>' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '        </div>';
                    $.fancybox(backData);

                    getPopup('.popup-ajax-link');
                } else {

                    console.log(data);

                    if (data.success) {
                        $.fancybox('<div style="width: 300px;text-align: center;padding: 25px 0 0 0">' + data.success + '</div>')
                    } else if (data.redirectUrl) {
                        window.location.replace(data.redirectUrl);
                    } else if (redirectUrl) {
                        window.location.replace(redirectUrl);
                    } else {
                        window.location.reload();
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var data = $.parseJSON(xhr.responseText);

                var items = [];

                $.each(data.errors, function (key, val) {
                    items.push('<li id="' + key + '">' + val + '</li>');
                });

                $('.formErrors').html(items);
            }
        });
    });
}

$.fn.serializeToObject = function () {
    var object = {}; //создаем объект
    var a = this.serializeArray(); //сериализируем в массив
    $.each(a, function () { //проходимся по массиву и добавляем параметр name как имя свойства объекта и value как его значение
        if (object[this.name] !== undefined) { //не забываем проверить данные
            if (!object[this.name].push) {
                object[this.name] = [object[this.name]];
            }
            object[this.name].push(this.value || '');
        } else {
            object[this.name] = this.value || '';
        }
    });
    return object;
};

/**
 * Won cases images
 * @param values
 */
function getWonCasesImages(values) {

    var images = [];

    $.each(values, function (index, value) {
        images.push(
            {href: '/storage/' + value}
        )
    });

    $.fancybox(images);
}
