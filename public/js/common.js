jQuery(function () {
    jQuery("#tabs").tabs();
});

jQuery(document).ready(function () {

    jQuery('.menu-btn').click(function () {
        jQuery(this).toggleClass('open');
        jQuery('header').fadeToggle('fast');
    });

    jQuery('.close_mod').click(function () {
        jQuery('.mod_bg').fadeOut('fast');
    });

    jQuery('.profile .edit').click(function () {
        jQuery(this).parent().find('input').removeAttr('disabled');
        jQuery(this).parent().removeClass('disabled');
    });

    jQuery('.form-with-types .choosen-type').val('Предпочтительный способо связи: Обратный звонок');

    jQuery('.form-with-types .c-item').click(function () {
        jQuery('.form-with-types .c-item').removeClass('checked');
        jQuery(this).addClass('checked');
        var temp_placeholder = jQuery(this).find(".placeholder").text();
        var type = jQuery(this).find(".type").text();
        jQuery(this).parent().parent().find('.choosen-type').val('Предпочтительный способо связи: ' + type);
        jQuery(this).parent().parent().find('.contact input').prop('placeholder', temp_placeholder);
    });

    jQuery('.btn.callback').click(function () {
        jQuery('.overlay.callback').fadeIn('fast');
    });

    jQuery('.overlay .close-btn').click(function () {
        jQuery(this).parent().parent().parent().fadeOut('fast');
    });

    jQuery('.btn.ask').click(function () {
        jQuery('.overlay.ask').fadeIn('fast');
    });
});


jQuery(document).mouseup(function (e) { // событие клика по веб-документу
    var div = jQuery(".mod_body"); // тут указываем ID элемента
    if (!div.is(e.target) // если клик был не по нашему блоку
        && div.has(e.target).length === 0) { // и не по его дочерним элементам
        jQuery('.mod_bg').fadeOut('fast');
    }
});

jQuery(document).ready(function () {
    jQuery('.slideshow').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        items: 1
    })
});

jQuery(document).ready(function () {
    jQuery('.slider-smi').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        items: 5,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })
});

jQuery(document).ready(function () {
    jQuery('.slider-dela').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        items: 1
    })
});

jQuery(document).ready(function () {
    jQuery('.slider-testimonials').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        items: 1
    })
});

jQuery(document).ready(function () {
    jQuery('.slider-partners').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        items: 5,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })
});

jQuery(document).ready(function () {
    jQuery('.slider-team').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        items: 4,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })
});
