modExtra.grid.Items = function(config) {
	config = config || {};
	Ext.applyIf(config,{
		id: 'modextra-grid-items'
		,url: modExtra.config.connector_url
        ,save_action: 'mgr/item/updatefromgrid'
        ,autosave: true
		,baseParams: {
			action: 'mgr/item/getlist'
		}
		,fields: ['id','user','obl_id','status','theme','question','answer','urist','gotovnost','log']
		,autoHeight: true
		,paging: true
		,remoteSort: true
		,columns: [
			{header: _('modextra_id'),dataIndex: 'id',width: 30}
            ,{header: _('modextra_obl_id'),dataIndex: 'obl_id',width: 50
                ,editor: { xtype: 'modExtra-combo-resources', renderer: true }
            }
            ,{header: _('modextra_status'),dataIndex: 'status',width: 50
            ,renderer: function(value){
                if( value == 1 ) { var color = 'red'; }
                else if( value == 5 ) { var color = '#DE4F3C'; }
                else if( value == 2 ) { var color = 'green'; }
                else if ( value == 4 ) { var color = 'blue';}
                else { var color = '#000'; }
                return '<span style="color:'+color+'">'+ _('modextra_status_'+value) +'</span>';
            }
            }
            ,{header: _('modextra_user'),dataIndex: 'user',width: 50}
			,{header: _('modextra_theme'),dataIndex: 'theme'}
			
		]
		,tbar: [
        /*{
			text: _('modextra_item_create')
			,handler: this.createItem
			,scope: this
		}*/
        /*,{ xtype: 'tbfill' }*/
        {
            xtype: 'modExtra-combo-status'
           ,id: 'modExtra-combo-status-filter'
           ,baseParams: {
                action:  'mgr/item/getlist'
            }
           ,listeners: {
                'select': {
                    fn: this.filter_status, scope:this
                }
                ,'render': function(){
                   var myArray=new Array()
                    myArray['id'] = 0;
                    myArray['name'] = _('modextra.filter.all');
                    var rec = new Ext.data.Record(myArray);
                    this.store.insert(0,rec);
                }
            }
            ,emptyText: _('modextra_status')
        }
        ,{
            xtype: 'modExtra-combo-resources'
           ,id: 'modExtra-combo-obl_id-filter'
           ,baseParams: {
                action:  'mgr/item/getlist'
            }
           ,listeners: {
                'select': {
                    fn: this.filter_obl_id, scope:this
                }
            }
            ,emptyText: _('modextra_obl_id')
        }
        ,{
            xtype: 'modExtra-combo-users'
           ,id: 'modExtra-combo-users-filter'
           ,baseParams: {
                action:  'mgr/item/getlist'
            }
           ,listeners: {
                'select': {
                    fn: this.filter_user, scope:this
                }
            }
            ,emptyText: _('modextra_user_title')
        }
            ,{
                xtype: 'modExtra-combo-urist'
                ,id: 'modExtra-combo-urist-filter'
                ,baseParams: {
                    action:  'mgr/item/getlist'
                }
                ,listeners: {
                    'select': {
                        fn: this.filter_urist, scope:this
                    }
                }
                ,emptyText: _('modextra_urist')
            }
        ,{
			text: _('modextra_filter_title')
			,handler: this.clearFilter
			,scope: this
		}
        ]
		,listeners: {
			/*rowDblClick: function(grid, rowIndex, e) {
				var row = grid.store.getAt(rowIndex);
				this.updateItem(grid, e, row);
			}*/
            viewready: function(){
                this.store.load(); // fix store resources
                //alert('ss')
                //Ext.getCmp('modExtra-combo-status-filter').getStore().add({ value: 'test', id: 0});
            }
		}
	});
	modExtra.grid.Items.superclass.constructor.call(this,config);
};
Ext.extend(modExtra.grid.Items,MODx.grid.Grid,{
	windows: {}

	,getMenu: function() {
		var m = [];
		m.push({
			text: _('modextra_item_update')
			,handler: this.updateItem
		});
		m.push('-');
		m.push({
			text: _('modextra_item_remove')
			,handler: this.removeItem
		});
		this.addContextMenuItem(m);
	}
	,clearFilter: function(cb){
        this.getStore().baseParams.status_id = 0;
        this.getStore().baseParams.user_id = 0;
        this.getStore().baseParams.obl_id = 0;
        this.getStore().baseParams.urist = 0;
       // Ext.getCmp('modExtra-combo-status-filter').getStore().add({id: '0', name: 'ALL'});
        this.getBottomToolbar().changePage(1);
        this.refresh()
        
       // .add({id: 0, name: 'sex'});
        
        Ext.getCmp('modExtra-combo-status-filter').setValue();
        Ext.getCmp('modExtra-combo-obl_id-filter').setValue();
        Ext.getCmp('modExtra-combo-users-filter').setValue();
        Ext.getCmp('modExtra-combo-urist-filter').setValue();
    }
	,createItem: function(btn,e) {
		if (!this.windows.createItem) {
			this.windows.createItem = MODx.load({
				xtype: 'modextra-window-item-create'
				,listeners: {
					'success': {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.createItem.fp.getForm().reset();
		this.windows.createItem.show(e.target);
	}

	,updateItem: function(btn,e,row) {
		if (typeof(row) != 'undefined') {this.menu.record = row.data;}
		var id = this.menu.record.id;

		MODx.Ajax.request({
			url: modExtra.config.connector_url
			,params: {
				action: 'mgr/item/get'
				,id: id
			}
			,listeners: {
				success: {fn:function(r) {
					if (!this.windows.updateItem) {
						this.windows.updateItem = MODx.load({
							xtype: 'modextra-window-item-update'
							,record: r
							,listeners: {
								'success': {fn:function() {
                                this.refresh();
                                },scope:this}
							}
						});
					}
					this.windows.updateItem.fp.getForm().reset();
					this.windows.updateItem.fp.getForm().setValues(r.object);
					this.windows.updateItem.show(e.target);
                    Ext.getCmp('modextra-user_name').update('<b>'+ _('modextra_user') +':</b> <a href="/manager/index.php?a=34&id='+r.object.user_id+'" target="_blank">'+r.object.user_name+'</a> ( '+r.object.user+' )')

				},scope:this}
			}
		});
	}

	,removeItem: function(btn,e) {
		if (!this.menu.record) return false;
		
		MODx.msg.confirm({
			title: _('modextra_item_remove')
			,text: _('modextra_item_remove_confirm')
			,url: this.config.url
			,params: {
				action: 'mgr/item/remove'
				,id: this.menu.record.id
			}
			,listeners: {
				'success': {fn:function(r) { this.refresh(); },scope:this}
			}
		});
	}
    ,filter_status: function(cb) {
            this.getStore().baseParams.status_id = cb.value;
            this.getBottomToolbar().changePage(1);
            this.refresh();
    }
    ,filter_obl_id: function(cb) {
            this.getStore().baseParams.obl_id = cb.value;
            this.getBottomToolbar().changePage(1);
            this.refresh();
    }
    ,filter_user: function(cb) {
            this.getStore().baseParams.user_id = cb.value;
            this.getBottomToolbar().changePage(1);
            this.refresh();
    },filter_urist: function(cb) {
        this.getStore().baseParams.urist = cb.value;
        this.getBottomToolbar().changePage(1);
        this.refresh();
    }
});
Ext.reg('modextra-grid-items',modExtra.grid.Items);




modExtra.window.CreateItem = function(config) {
	config = config || {};
	this.ident = config.ident || 'mecitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('modextra_item_create')
		,id: this.ident
		,autoHeight: false
		,width: 750
        ,layout: 'form'
		,modal: true
		,height: Ext.getBody().getViewSize().height*.7
		,autoScroll: true
		,url: modExtra.config.connector_url
		,action: 'mgr/item/create'
		,fields: [
			{xtype: 'textfield',fieldLabel: _('modextra_theme'),name: 'theme',id: 'modextra-'+this.ident+'-theme',anchor: '99%'}
			,{xtype: 'textarea',fieldLabel: _('modextra_question_title'),name: 'question',id: 'modextra-'+this.ident+'-question',height: 150,anchor: '99%'}
		]
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	modExtra.window.CreateItem.superclass.constructor.call(this,config);
};
Ext.extend(modExtra.window.CreateItem,MODx.Window);
Ext.reg('modextra-window-item-create',modExtra.window.CreateItem);


modExtra.window.UpdateItem = function(config) {
	config = config || {};
	this.ident = config.ident || 'meuitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('modextra_item_update')
		,id: this.ident
        
        ,modal: true
		,autoHeight: false
        ,height: Ext.getBody().getViewSize().height*.7
        ,width: 750
        ,autoScroll: true
		
        //,layout: 'form'
		
		
		,url: modExtra.config.connector_url
        ,action: 'mgr/item/update'
		,fields: [

                    {xtype: 'hidden',name: 'id',id: 'modextra-update-id'}
                    ,{
                         xtype: 'modExtra-combo-status'
                         ,fieldLabel: _('modextra_status')
                         ,name: 'status'
                         ,hiddenName: 'status'
                         ,anchor: '100%'
                     }
                    ,{
                        xtype: 'datefield'
                        ,fieldLabel: _('modextra_gotovnost')
                        ,name: 'gotovnost'
                        ,hiddenName: 'gotovnost'
                        ,anchor: '100%'
                        ,format: 'Y-m-d'
                    }
                     ,{
                         xtype: 'modExtra-combo-resources'
                         ,fieldLabel: _('modextra_obl_title')
                         ,name: 'obl_id'
                         ,hiddenName: 'obl_id'
                         ,anchor: '100%'
                     }
                     ,{
                        xtype: 'modExtra-combo-urist'
                        ,fieldLabel: _('modextra_urist')
                        ,name: 'urist'
                        ,hiddenName: 'urist'
                        ,anchor: '100%'
                    }
                    ,{
                        html: ''
                        ,border: false
                        ,id: 'modextra-user_name'
                        ,cls: 'modx-page-header container'
                    }

                    ,{
                        xtype: 'numberfield'
                         ,fieldLabel: _('modextra.price')
                         ,name: 'total_pay'
                         ,hiddenName: 'total_pay'
                         ,anchor: '100%'
                         ,decimalPrecision:2
                    }
                    ,{
                        xtype: 'xcheckbox',
                        fieldLabel: _('modextra.payed'),
                        name: 'payed',
                        hiddenName: 'payed',
                        inputValue:1
                    }

                    ,{xtype: 'textfield',fieldLabel: _('modextra_theme'),name: 'theme',id: 'modextra-'+this.ident+'-theme',anchor: '99%'}
                    ,{xtype: 'textarea',fieldLabel: _('modextra_question_title'),name: 'question',id: 'modextra-'+this.ident+'-question',height: 250,anchor: '99%'}
                    ,{
                        html: '<h4>'+_('modextra.files_user_title')+'</h4>'
                        ,border: false
                        ,cls: 'modx-page-header container'
                    }
                    ,{
                        xtype: 'modextra-grid-files'
                        ,id: 'modextra-grid-files-fromUser'
                        ,defaults: { border: false ,autoHeight: true }
                        ,preventRender: true
                    }
                    
                    ,{xtype: 'textarea',fieldLabel: _('modextra_answer_title'),name: 'answer',id: 'modextra-'+this.ident+'-answer',height: 250,anchor: '99%'}
                    ,{
                        html: '<h4>'+_('modextra.files_manager_title')+'</h4>'
                        ,border: false
                        ,cls: 'modx-page-header container'
                    }
                    ,{
                        xtype: 'modextra-grid-files'
                        ,id: 'modextra-grid-files-fromManager'
                        ,defaults: { border: false ,autoHeight: true }
                        ,preventRender: true
                    },
            {xtype: 'hidden',name: 'log',id: 'modextralog'}


		]
		,keys: []
        ,expand: function() {
           // Ext.getCmp('modextra-user_name').update('test')
            var id = Ext.getCmp('modextra-update-id').getValue();
            /* load users files */
            files_user = Ext.getCmp('modextra-grid-files-fromUser');
            files_user.getStore().setBaseParam('id',id);
            files_user.getStore().setBaseParam('type',1);
            files_user.store.load({ params:{ id: id, type: 1 } });
            /* load manager files */
            files_manager = Ext.getCmp('modextra-grid-files-fromManager');
            files_manager.getStore().setBaseParam('id',id);
            files_manager.getStore().setBaseParam('type',0);
            files_manager.store.load({ params:{ id: id, type: 0 } });
        }
        ,success: function(r){
            var id = Ext.getCmp('modextra-update-id').getValue();

            MODx.msg.confirm({
               title: _('modextra.sendemail.title'),
               text: _('modextra.sendemail.text'),
               url: modExtra.config.connector_url,
               params: {
                  action: 'mgr/item/sendemail'
                  ,id: id
                  
               },
               listeners: {
                    'success':{fn: function(r) {
                    },scope:true}
               }
            });
        }
	});
	modExtra.window.UpdateItem.superclass.constructor.call(this,config);
};
Ext.extend(modExtra.window.UpdateItem,MODx.Window);
Ext.reg('modextra-window-item-update',modExtra.window.UpdateItem);


modExtra.combo.Resources = function(config) {
    config = config || {};
    Ext.applyIf(config,{
        store: new Ext.data.ArrayStore({
            //id:  'modExtra-combo-section'
            fields: [ 'id','name' ]
            ,url: modExtra.config.connector_url
            ,baseParams: {
                action: 'mgr/item/getresources',
                combo: true
            }
        })
        ,mode: 'remote'
        ,displayField: 'name'
        ,valueField: 'id'
        ,pageSize: 20
    });
    modExtra.combo.Resources.superclass.constructor.call(this,config);
};
Ext.extend(modExtra.combo.Resources,MODx.combo.ComboBox);
Ext.reg('modExtra-combo-resources',modExtra.combo.Resources);

modExtra.combo.Urist= function(config) {
    config = config || {};
    Ext.applyIf(config,{
        store: new Ext.data.ArrayStore({
            //id:  'modExtra-combo-section'
            fields: [ 'id','name' ]
            ,url: modExtra.config.connector_url
            ,baseParams: {
                action: 'mgr/item/geturist',
                combo: true
            }
        })
        ,mode: 'remote'
        ,displayField: 'name'
        ,valueField: 'id'
        ,pageSize: 20
    });
    modExtra.combo.Urist.superclass.constructor.call(this,config);
};
Ext.extend(modExtra.combo.Urist,MODx.combo.ComboBox);
Ext.reg('modExtra-combo-urist',modExtra.combo.Urist);


modExtra.combo.Users = function(config) {
    config = config || {};
    Ext.applyIf(config,{
        store: new Ext.data.ArrayStore({
            //id:  'modExtra-combo-section'
            fields: [ 'id','name' ]
            ,url: modExtra.config.connector_url
            ,baseParams: {
                action: 'mgr/item/getusers',
                combo: true
            }
        })
        ,mode: 'remote'
        ,displayField: 'name'
        ,valueField: 'id'
        ,pageSize: 20
    });
    modExtra.combo.Users.superclass.constructor.call(this,config);
};
Ext.extend(modExtra.combo.Users,MODx.combo.ComboBox);
Ext.reg('modExtra-combo-users',modExtra.combo.Users);

modExtra.combo.Status = function(config) {
    config = config || {};
    Ext.applyIf(config,{
        store: new Ext.data.ArrayStore({
            //id:  'modExtra-combo-section'
            fields: [ 'id','name' ],
            data: [
                [5, _('modextra_status_5')],
                [1, _('modextra_status_1')],
                [4, _('modextra_status_4')],
                [2, _('modextra_status_2')],
                
                [3, _('modextra_status_3')]
            ]
        })
        ,mode: 'local'
        ,displayField: 'name'
        ,valueField: 'id'
    });
    modExtra.combo.Status.superclass.constructor.call(this,config);
};
Ext.extend(modExtra.combo.Status,MODx.combo.ComboBox);
Ext.reg('modExtra-combo-status',modExtra.combo.Status);



// grid files
modExtra.grid.files = function(config) {
    config = config || {};

    Ext.applyIf(config,{
       // id: 'modextra-grid-files'
        url: modExtra.config.connectorUrl
        ,baseParams: { action: 'mgr/files/getlist' }
        ,fields: ['id','type','quest_id','path','name']
        ,paging: true
        ,remoteSort: true
        ,width: "100%"
        ,autoExpandColumn: 'id'
        ,tbar:[
            {
                text: _('modextra.files_upload')
               // ,handler: this.createItem
                ,scope: this
                ,handler:function(btn){
                    this.createItem(this.id)
                }
            }
        ]
        ,columns: [
            {
                header: _('modextra.files_path')
                ,dataIndex: 'path'
                ,sortable: false
                ,renderer: function(value){
                    return '<a href="'+value+'" target="_blank">'+ value +'</a>';
                }
                //,hidden: true
            }
            ,{
                header: _('name')
                ,dataIndex: 'name'
                ,width: 100
                ,sortable: false
            }

		]
        ,getMenu: function() {
            var m = [];
            m.push({
                text: _('modextra_item_update')
                ,handler: this.updateItem
            });
            m.push('-');
            m.push({
                text: _('modextra_item_remove')
                ,handler: this.removeItem
            });
            this.addContextMenuItem(m);
        }
    });
    modExtra.grid.files.superclass.constructor.call(this,config)
};
Ext.extend(modExtra.grid.files,MODx.grid.Grid,{
        createItem: function(btn,e) {
            var upload_from = this.id;
            var new_id = Ext.getCmp('modextra-update-id').getValue();
            var path = 'components/modextra/dl/'+new_id+'/';
            //alert(path)
            if( upload_from == 'modextra-grid-files-fromUser' ) {
                var type = 1;
            } else {
                var type = 0
            }
            
            //if (!this.uploader) {
                this.uploader = new Ext.ux.UploadDialog.Dialog({
                    url: modExtra.config.connector_url //MODx.config.connectors_url+'browser/file.php'
                    ,base_params: {
                        action: 'mgr/files/upload'
                        ,path: path
                        ,type: type
                        ,quest_id: new_id
                        ,wctx: MODx.ctx || ''
                        ,source: ''
                    }
                    ,reset_on_hide: true
                    ,width: 550
                    ,cls: 'ext-ux-uploaddialog-dialog modx-upload-window'
                    ,listeners: {
                        show: function(){
                        }
                        ,uploadsuccess: function(){
                            Ext.getCmp(upload_from).getStore().reload();
                        }
                        ,uploaderror: function(){}
                        ,uploadfailed: function(){}
                    }
                });
            //}
            this.uploader.show(btn);
            
        }

        ,updateItem: function(btn,e,row) {
            if (typeof(row) != 'undefined') {this.menu.record = row.data;}
            var id = this.menu.record.id;

            MODx.Ajax.request({
                url: modExtra.config.connector_url
                ,params: {
                    action: 'mgr/files/update'
                    ,id: id
                }
                ,listeners: {
                    success: {fn:function(r) {
                        if (!this.windows.updateFile) {
                            this.windows.updateFile = MODx.load({
                                xtype: 'modextra-window-file-update'
                                ,record: r
                                ,listeners: {
                                    'success': {fn:function() { this.refresh(); },scope:this}
                                }
                            });
                        }
                        this.windows.updateFile.fp.getForm().reset();
                        this.windows.updateFile.fp.getForm().setValues(r.object);
                        this.windows.updateFile.show(e.target);

                    },scope:this}
                }
            });
        }
        ,removeItem: function(btn,e) {
            if (!this.menu.record) return false;
            
            MODx.msg.confirm({
                title: _('modextra_item_remove')
                ,text: _('modextra.files_remove_confirm')
                ,url: this.config.url
                ,params: {
                    action: 'mgr/files/remove'
                    ,id: this.menu.record.id
                    ,path: this.menu.record.path
                }
                ,listeners: {
                    'success': {fn:function(r) { this.refresh(); },scope:this}
                }
            });
        }

});
Ext.reg('modextra-grid-files',modExtra.grid.files);


modExtra.window.UpdateFile = function(config) {
	config = config || {};
	this.ident = config.ident || 'file'+Ext.id();
	Ext.applyIf(config,{
		title: _('modextra_item_update')
		,id: this.ident
        ,modal: true
		,autoHeight: true
        ,width: 300
        ,autoScroll: false
		,url: modExtra.config.connector_url
        ,action: 'mgr/files/update'
		,fields: [
            {xtype: 'hidden',name: 'id',id: 'modextra-file-id'}
            ,{
                xtype: 'textfield',
                fieldLabel: _('files_file'),
                name: 'name',
                id: 'modextra-'+this.ident+'-name',
                anchor: '99%'
            }
		]
		,keys: []
        ,expand: function() {
        }
        ,success: function(){
           // alert('sex')
            files_user.store.load();
            files_manager.store.load();
        }
        
	});
	modExtra.window.UpdateFile.superclass.constructor.call(this,config);
};
Ext.extend(modExtra.window.UpdateFile,MODx.Window);
Ext.reg('modextra-window-file-update',modExtra.window.UpdateFile);
/*

modExtra.window.UploadFile = function(config) {
	config = config || {};
	this.ident = config.ident || 'file'+Ext.id();
	Ext.applyIf(config,{
		title: _('modextra_item_update')
		,id: this.ident
        ,modal: true
		,autoHeight: true
        ,width: 300
        ,autoScroll: false
		,url: modExtra.config.connector_url
        ,action: 'mgr/files/update'
		,fields: [
            {xtype: 'hidden',name: 'id',id: 'modextra-file-id'}
            
		]
		,keys: []
        ,expand: function() {
        }
        ,success: function(){
           // alert('sex')
           // files_user.store.load();
          //  files_manager.store.load();
        }
        
	});
	modExtra.window.UploadFile.superclass.constructor.call(this,config);
};
Ext.extend(modExtra.window.UploadFile,MODx.Window);
Ext.reg('modextra-window-file-upload',modExtra.window.UploadFile);*/