/*
    перекрывашка beta
    $('.do-something').gloaders();
    $('#form_request input[type="submit"]').gloaders({padding: 50});
    $("#strot").gloaders('deny');
*/

(function( $ ){
var defaults = {
    padding: 10,
    cls: 'overloader',
    cls_container: 'gloader_container',
    cls_image: 'g_overloader_image',
    loader_height: 64,
    loader_width: 64,
    sheetWidth: 768,
    sheetHeight: 64,
    frames: 12,
    frameRate: 20
}
var methods = {
    init: function(params) {
        var options = $.extend({}, defaults, params);
        
        return this.each(function(){
            // make padding
            var parent = $(this).parents("."+options.cls_container);
            options.height = parent.height() + options.padding*2;
            options.width  = parent.width()  + options.padding*2;

            parent.find('.'+options.cls).remove();
            parent.prepend( '<div class="'+options.cls+'" style="position: absolute;'+
                'height: '+options.height+'px;'+
                'width: '+options.width+'px;'+
                'margin-left: -'+options.padding+'px;'+
                'margin-top: -'+options.padding+'px;'+
                'min-height: '+options.loader_height+'px;'+
                'min-width: '+options.loader_width+'px;'+
                '"><div class="'+options.cls_image+'"></div></div>' );
            $(parent).find('.'+options.cls_image).css({
                'left':'50%',
                'top':'50%',
                'margin-left':'-'+ Math.round( options.loader_width/2 )+'px',
                'margin-top':'-'+ Math.round( options.loader_height/2 )+'px'
                }).sprite({
                    sheetWidth: options.sheetWidth,
                    sheetHeight: options.sheetHeight,
                    frames: options.frames,
                    frameRate: options.frameRate
                }).sprite('play');
        });
    },
    deny_in: function(params) {
        var options = $.extend({}, defaults, params);
        $(this).find('.'+options.cls).remove();
        return this;
    },
    deny_from: function(params) {
        var options = $.extend({}, defaults, params);
        $(this).parents('.'+options.cls_container).find('.'+options.cls).remove();
        return;
    }
}
 
$.fn.gloaders = function(method){
    if ( methods[method] ) {
        return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
        return methods.init.apply( this, arguments );
    } else {
        $.error( 'Метод "' +  method + '" не найден в плагине jQuery.gloaders' );
    }
}
})( jQuery );