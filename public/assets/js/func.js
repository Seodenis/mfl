$(document).ready( function() {

    $('section.uslugi .menu li:nth-child(4n) a').css({'margin-right': '0px', 'float': 'right'})
    function jpane_ini(){
        
                jpane = $('.scroll_pane:not(.noscroll)').jScrollPane({
                    horizontalDragMinWidth: 22,
                    horizontalDragMaxWidth: 22,
                    verticalDragMaxHeight: 22,
                    verticalDragMinHeight: 22,
                    hijackInternalLinks: true,
                    showArrows: true,
                });
                
                jpane_api = jpane.data('jsp');
                if( typeof(jpane_api) != 'undefined' ) {
                jpane_api.reinitialise();
                }

                
    }
    jpane_ini();
    
    $('table td .show_more').live('click', function(){
        var num = parseInt( $(this).parents('tr').find('td.num').html() );
        var tr = $('table tr[data-id="'+num+'"]');
        $(tr).toggleClass('hide');
        if( $(tr).hasClass('hide') ) { $(this).html('Развернуть')}
        else { $(this).html('Свернуть') }
        jpane_ini();
    });
    
    if( $('#add_file').length > 0 ) {
        $('.files .in .one.new').removeClass('new');
        $('.files .in').prepend('<div class="one new"><input type="file" name=file[] /><span class="del" title="удалить"></span></div>');
        jpane_ini();
        $(".one.new input:file").uniform({fileDefaultText: '...',fileBtnText: 'Прикрепите файл'});
    }
    $('#add_file').live('click', function(){
        var last_inp = $('.files .in .one:last').find('input');
        if( $(last_inp).val() == '' ) {
            $(last_inp).click();
        }else {
        $('.files .in .one.new').removeClass('new');
        $('.files .in').append('<div class="one new"><input type="file" name=file[] /> <span class="del" title="удалить"></span></div>');
        jpane_ini();
        $(".one.new input:file").uniform({fileDefaultText: '...',fileBtnText: 'Прикрепите файл'});
        var last_inp = $('.files .in .one:last').find('input');
        $(last_inp).click();
        }
    });
    $('.files .in .one .del').live('click',function(){
        $(this).parents('.one').remove();
        jpane_ini();
    });
    
    
    $('#pre_load').sprite({
                    sheetWidth: 768,
                    sheetHeight: 64,
                    frames: 12,
                    frameRate: 20
                }).sprite('play');
                
    $('header.header .buts a:not(.noAjax), .vozvrat a:not(.noAjax), section.uslugi a:not(.noAjax), section.blocks .news a:not(.noAjax), .authPanel a:not(.noAjax), .controls a:not(.noAjax), #content a:not(.noAjax), .info a,.akcia_but a.buton_orange').live('click', function(){

        var data_news = $(this).data('news-id');
        
        var link = $(this).attr('href');
        $('#overlay, #pre_load').show();
        $('#content').css({'z-index': 1});
        //alert(link)
        
        var data = new Array();
        $.post( link, data, function(result){
            var content = $(result).filter("#content").html();
            
            $('#content').html(content).show().css({'z-index': 100500});
            $(window).scrollTop(0);
            if( $('#add_file').length > 0 ) {
                $('.files .in .one.new').removeClass('new');
                $('.files .in').prepend('<div class="one new"><input type="file" name=file[] /><span class="del" title="удалить"></span></div>');
                jpane_ini();
                $(".one.new input:file").uniform({fileDefaultText: '...',fileBtnText: 'Прикрепите файл'});
            }
            
            if( typeof(data_news) != 'undefined') {
                 $('.one_article_more[id="id_'+data_news+'"]').find('.text').slideToggle(100,function(){
                    jpane_ini();
                });
                //alert(test)
            }
            jpane_ini();
        });
        
        
        return false;
    });
    
    $('.one_article_more a.tits').live('click',function(){
        $(this).parents('.one_article_more').find('.text').slideToggle(100,function(){
            jpane_ini();
        });
        return false;
    });

    $('#content .title .close, #overlay').live('click',function(){
        $('#content, #overlay, #pre_load').hide();
        return false;
    });
    
    
    /* viktorina */
    /*
    setTimeout(function(){
        $('.akcia_but a.buton_orange').addClass('blink')
    }, 2000);
    */
    setInterval(function (){$('.akcia_but a.buton_orange').toggleClass('blink')},300)
    
    /* route */
    function routes_main( ){
      
        setTimeout(function(){
            var slider = $('#route');
            if( $('#route .one.zadat').hasClass('active') ) {
                $('#route .one.zadat').removeClass('active');
                $('#route .one.zadat').stop(true,true).animate({ marginLeft: '-223px'}, 300 );
            } else {
                $('#route .one.zadat').addClass('active');
                $('#route .one.zadat').stop(true,true).animate({marginLeft: '0px'}, 300 );
            }
            
            routes_main();
        }, 5000);
    }
    routes_main();
    
    
   /* make formit overlap and clever ajax */
    $('#form_request input[type="submit"]').live("click",function(){
        var button = $(this);
        $(button).gloaders();
        // default
        var link = $(this).parents("#form_request").attr("action");
        var form = $("#form_request").serializeArray();
        
        $.post( site_url+link, form, function(data){
                var errors = 0;
                $(form).each(function (i, arr) {
                    //alert(arr.name+'..'+arr.value)
                    var ferror = $(data).find('#form_request [name="'+arr.name+'"]').hasClass('ferror');
                    if( ferror == true || typeof(ferror) == 'undefined' ) {
                        errors++;
                        $('#form_request [name="'+arr.name+'"]').addClass('ferror').val(arr.value);
                    } else {
                        $('#form_request [name="'+arr.name+'"]').removeClass('ferror').val(arr.value);
                    }
                });
                if( errors != 0 ) {
                    $('#form_request .error').show();
                    $('#form_request .sucmes').hide();
                } else {
                    $('#form_request .error').hide();
                    $('#form_request .sucmes').show();
                }
                $(button).gloaders('deny_from');
        });
        
        return false;
    });
   
   /* mask phone */
   $("input.phone").mask("(999) 999-9999");
   
   /* resize textarea */
   $('textarea:not(.nonanimate)').autosize({append: "\n"});

   /* title show/hide on focus/blur */
   function values(){
      $('input[type="text"], textarea, input[type="password"]').bind("focus", function(e){
         var title = $(this).attr("title");
         var value = $(this).attr("value");
         if ( typeof(title) != 'undefined' && title != '' && title == value ) {
            $(this).val("").removeClass("disable");
         }
      });

      $('input[type="text"], textarea, input[type="password"]').bind("blur", function(e){
         var title = $(this).attr("title");
         var value = $(this).attr("value");
         if ( typeof(title) != 'undefined' && title != '' && value == '' ) {
            $(this).val(title).addClass("disable");
         }
      });

      $('input[type="text"], textarea, input[type="password"]').each(function (i) {
         var title = $(this).attr("title");
         var value = $(this).attr("value");
         if( typeof(title) != 'undefined' && title != '' && value == '' ) {
            $(this).val(title).addClass("disable");
         }
      });
   }
   values();
});