<!DOCTYPE html>
<html>
<head>
    [[$meta_head]]
[[-!MetaX?tpl=`metax-html5`]]
[[$head]]
</head>
<body>
[[$header]]
[[$breadcrumbs]]
<div class="heading_title">
        <h1>Как мы работаем</h1>
</div>
<section class="carusel_rabota">
    <div class="wrap">
        <div class="sliderkit" id="carousel">
            <div class="sliderkit-nav">
                <div class="sliderkit-nav-clip">
                    <ul>
                    [[parseMIGX?tvname=`uslugi_glav`  &tpl=`migx_uslugi_glav`]]
                    </ul>
                </div>
                <div class="sliderkit-btn sliderkit-nav-btn sliderkit-nav-prev">
                    <a href="#" title="Предыдущий"></a>
                </div>
                <div class="sliderkit-btn sliderkit-nav-btn sliderkit-nav-next">
                    <a href="#" title="Следующий"></a>
                </div>
            </div>
        </div>

    </div>
</section>

[[if_evo? &is=`[[parseMIGX?tvname=`preimujestva_show`  &tpl=`migx_show_block` &maxcount=1]]:is:да:` &then=`
<section class="preimujestva">
    <div class="wrap">
        <div class="zag">
                   <div class="zag_text">
                        <div class="line line_l"></div>
                        <h2>[[parseMIGX?tvname=`preimujestva_show`  &tpl=`migx_show_zag` &maxcount=1]]</h2>
                        <div class="line line_r"></div>
                   </div>
        </div>
        <div class="el_list">
        [[parseMIGX?tvname=`preimujestva`  &tpl=`migx_preimujestva`]]
        </div>
    </div>
</section>
`]]
[[if_evo? &is=`[[parseMIGX?tvname=`zakaz_uslugi_show`  &tpl=`migx_show_block` &maxcount=1]]:is:да:` &then=`
<section class="uslugi">
    <div class="wrap">
        <div class="zag">
            <div class="zag_text">
                <div class="line line_l"></div>
                 <h2>[[parseMIGX?tvname=`zakaz_uslugi_show`  &tpl=`migx_show_zag` &maxcount=1]]</h2>
                <div class="line line_r"></div>
            </div>
            </h2>
        </div>
            <div class="el_list">
            [[parseMIGX?tvname=`zakaz_uslugi`  &tpl=`migx_zakaz_uslugi`]]
        </div>
    </div>
</section>
`]]

<section class="cifri">
    <div class="wrap">
        <div class="zag">
            <div class="zag_text">
                <div class="line line_l"></div>
                <h2>[[parseMIGX?tvname=`uris_v_cifrah_show`  &tpl=`migx_show_zag` &maxcount=1]]</h2>
                <div class="line line_r"></div>
            </div>
        </div>
        <div class="el_list">
        [[parseMIGX?tvname=`uris_v_cifrah`  &tpl=`migx_uris_v_cifrah`]]
        </div>
    </div>
</section>
<section class="garant">
    <div class="wrap">
        <div class="zag">
            <h2>Наши гарантии</h2>
        </div>
        <div class="el_list">
            <div class="el">
                <img src="img/garant_1.png">
                <div class="text">выигранных споров</div>
                <div class="opis">Все сертифицированные юристы проекта проходят обязательную проверку образования и опыта работы.</div>
                <div class="razdel"></div>
            </div>
            <div class="el">
                <img src="img/garant_2.png">
                <div class="text">Гарантия получения услуги</div>
                <div class="opis">Нами открыт депозит в raiffeisen bank, позволяющий вернуть оплаченную вами сумму в случае неполучения услуги.</div>
                <div class="razdel"></div>
            </div>
            <div class="el">
                <img src="img/garant_3.png">
                <div class="text">КОНФИДЕНЦИАЛЬНОСТЬ</div>
                <div class="opis">Ваши персональные данные нигде не публикуются. Передача любой информации защищена сертификатом SSL.</div>
            </div>
        </div>
    </div>
</section>
<section class="info_list wrap">
        <div class="list news">
            <div class="zag">Новости</div>

            <div class="news_el">
                <div class="date">22.01.2015</div>
                <a href="#">Мой Семейный Юрист помогает
                молодым мамам</a>
                Все чаще к нашим юристам обращаются молодые родители, у которых с появлением малыша возникает ряд непростых…
            </div>
            <div class="news_el">
                <div class="date">22.01.2015</div>
                <a href="#">Перерыв в работе стойки в МЕГА Химки</a>
                Друзья, в связи с технической необходимостью реконструкции стойки экспресс-консультаций в МЕГА Химки. . .
            </div>
            <div class="news_el">
                <div class="date">22.01.2015</div>
                <a href="#">Продление акции</a>
                Друзья,  нам очень приятно поздравить Вас с Новогодними праздниками! Спасибо за Ваше доверие к нам и…
            </div>

            <div class="all_info"><a href="#">Все новости</a> &#62;</div>
        </div>

    <div class="list otziv">
        <div class="zag">Отзывы</div>

        <div class="news_el">
        <a href="#">Валентина</a>
        Доброго дня! Благодарю за профессионализм и оперативность. Юридическое Сопровождение до и во время судебного заседания высокого уровня. Результат - иск удовлетворен. Цены на услуги приятно удивили.
    </div>
        <div class="news_el">
            <a href="#">Яна Ковтуновская</a>
            Добрый день. Хочу выразить огромную благодарность Вашей профессиональной команде. Помощь в моем вопросе была оказана в короткие сроки, в нескольких
        </div>
        <div class="news_el">
            <a href="#">Елена</a>
            Здравствуйте! Консультацию от Вас получила. Спасибо. Мне она поможет.
        </div>

        <div class="all_info"><a href="#">Все новости</a> &#62;</div>
    </div>

    <div class="list statyi">
        <div class="zag">Статьи</div>

        <div class="news_el">
            <a href="#" class="rubrik">Название рубрики</a>
            <a href="#" class="stat">Заголовок</a>
            Все чаще к нашим юристам обращаются молодые родители, у которых с появлением малыша возникает ряд непростых…
        </div>
        <div class="news_el">
            <a href="#" class="rubrik">Название рубрики</a>
            <a href="#" class="stat">Заголовок, название заголовка...</a>
            Все чаще к нашим юристам обращаются молодые родители, у которых с появлением малыша возникает ряд непростых…
        </div>
        <div class="news_el">
            <a href="#" class="rubrik">Название рубрики</a>
            <a href="#" class="stat">Заголовок, название заголовка...</a>
            Все чаще к нашим юристам обращаются молодые родители, у которых с появлением малыша возникает ряд непростых…
        </div>

        <div class="all_info"><a href="#">Все новости</a> &#62;</div>
    </div>
</section>

<article class="content wrap">
[[*content]]
</article>

[[$footer]]
[[$scripts]]
[[$shetchik]]
[[$zayvka]]
</body>
</html>