
$(document).ready(function(){
	var city = $('#city').val();
	$('form').append('<input type="hidden" name="city" value="'+city+'">');
	
	
	if(screen.width > 500) {
		$('input[name=phone],input[name=tel]').mask('+7 (999) 999-99-99');
	}
	
	$('.colorbox').colorbox({
		maxWidth:'80%',
		current:'Страница {current} из {total}'
	});
	

	$('.fancybox1').fancybox({
		padding: 0, //убираем отступ
		helpers: {
		overlay: {
			locked: false // отключаем блокировку overlay
		}
	}});
	$('.title_keis').click(function(){
		var parent = $(this).parents('.keys_page');
		parent.find('.keys_preview').find('a').eq(0).click();
	});
	
	
    $('.slider').owlCarousel({
        dots:false,
        nav:true,
        loop:true,
        items:1,
        navSpeed:600,
		autoplay:true,
		autoplayTimeout:6000
    });
	setTimeout(function(){
		$('.home_slider').css('opacity','1');
	},300);
    $('.objects').owlCarousel({

        dots:false,
        nav:true,
        loop:true,
        navSpeed:600,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				nav:true
			},
			600:{
				items:4
			},
			800:{
				items:6
			},
			1051:{
				items:8
			}
		}	
    });
	$('.home_partners').owlCarousel({

        dots:false,
        nav:true,
        loop:true,
        navSpeed:600,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				nav:true
			},
			600:{
				items:3
			},
			800:{
				items:5
			},
			1051:{
				items:5
			}
		}	
    });
    $('.reviews-content').owlCarousel({
        items:1,
        dots:false,
        nav:true,
        loop:true
    });
    $('.objects .object a').hover(function(){
        var img = $(this).find('img').attr('src');
        var hover = $(this).attr('data-hover');
        $(this).find('img').attr('src',hover);
        $(this).attr('data-hover',img);
    },function(){
        var img = $(this).find('img').attr('src');
        var hover = $(this).attr('data-hover');
        $(this).find('img').attr('src',hover);
        $(this).attr('data-hover',img);
    });
    $('.service_list .item').click(function(){
		if($(this).hasClass('active') == false) {
			var img = $(this).find('img').attr('data-hover');
			var img2 = $(this).find('img').attr('src');
			var target = $(this).attr('data-target');
			$('.service_list .item.active').each(function(){
				var img3 = $(this).find('img').attr('data-hover');
				var img4 = $(this).find('img').attr('src');
				$(this).find('img').attr('src',img3);
				$(this).find('img').attr('data-hover',img4);
				$(this).removeClass('active');
			});
			$('.service_block').hide();
			$('.service_block[data-show=target'+target+']').fadeIn(0);
			$(this).find('img').attr('src',img);
			$(this).find('img').attr('data-hover',img2);
			$(this).addClass('active');
		}
        
    });
	$('.service_list .item').eq(0).addClass('active');
	
	
	
	
	$('.test-item').eq(0).show();
	var test_count = $('.test-item').length;
	$('.count_t').text(test_count);
	$('#next_step').click(function(){
		var next = parseInt($(this).attr('data-step'));
		if(next <= test_count) {
			$('.test-item').hide();
			$('.test-item').eq(next-1).show();
			$('.index_t').text(next);
			$(this).attr('data-step',next+1);
		}
		else {
			var sum = 0;
			$('.result_tpl').hide(300);
			$('.test-item input:checked').each(function(){
				sum += parseInt($(this).val());
			});
			$('.test-item').parent().fadeOut(300);
			$('.result_tpl').each(function(){
				var from = parseInt($(this).attr('data-from'));
				var to = parseInt($(this).attr('data-to'));
				if(sum >= from && sum <=to) {
					$(this).show(300);
					$('.subscribe').fadeIn(300);
				}
				
			});
			
		}
		
		return false;
	});
	
	$('.payment_type .pay').click(function(){
		var type = $(this).attr('data-type');
		$('input[name=paymentType]').val(type);
		return false;
	});
	
	
	
	
	
	/*$('.payment_form').submit(function(){
		var summ = $('#zayv_summ').val();
		var name = $('#zayv_fio').val();
		var email = $('#zayv_summ').val();
		if($('#zayv_tel').val() != '' && $('#zayv_tel').val() != undefined) {
			var phone = $('#zayv_tel').val();
		}
		else {
			var phone = 0;
		}
		$('#yandex_money input[name=sum]').val(summ);
		$('#yandex_money input[name=customerNumber]').val(name);
		$('#yandex_money input[name=cps_email]').val(email);
		if(phone != 0) {
			$('#yandex_money input[name=cps_phone]').val(phone);
		}
		$('#yandex_money').submit();
		return false;
	});
	
	*/
	
	
});


function func_in() {
	$(document).ready(function(){
		
	});
}

// Roistat Begin
// Calltracking Begin
$(document).ready(function() {
    // Header
    (function() {
        var phone = $('.phone_panel').find('p');

        phone.addClass('roistat-phone-tel');
        phone.html('<span>+<string class="roistat-phone-country">7</string> (<string class="roistat-phone-region">499</string>)</span> <string class="roistat-phone-number">703 03 03</string>');

    })();

    // Body
    (function() {
        var phone =$('.wrapper > h4');

        phone.addClass('roistat-phone-tel');
        phone.html('+<string class="roistat-phone-country">7</string> (<string class="roistat-phone-region">499</string>) <span><string class="roistat-phone-number">703 03 03</string></span>');
    })();

    // Footer
    (function() {
        var phone = $('.footer_phone');

        phone.addClass('roistat-phone-tel');
        phone.html('+<string class="roistat-phone-country">7</string> (<string class="roistat-phone-region">499</string>) <span><string class="roistat-phone-number">703 03 03</string></span>');
    })();

    // Pages
    switch(window.location.pathname) {
        case '/kontaktyi.html':
            $('.contakt').eq(0).html('+<string class="roistat-phone-country">7</string> <string class="roistat-phone-region">499</string> <string class="roistat-phone-number">703 03 03</string>');
            $('.contakt').eq(0).addClass('roistat-phone-tel');
            break;
    }
});
// Calltracking End
// Integration Begin
if(window.location.pathname === '/vxod/registracziya.html') {
    $('.form').submit(function() {
        var form = 'Регистрация';
        var name = $(this).find('input[placeholder="Имя"]').val();
        var email = $(this).find('input[placeholder="E-mail"]').val();

        roistatGoal.reach({
            leadName: 'Заявка с "' + form + '"',
            name    : name,
            phone   : null,
            email   : email,
            fields  : {
                form_name     : form,
                type_treatment: form,
                city          : '{city}',
                current_page  : '{landingPage}',
                utmSource     : '{utmSource}',
                utmMedium     : '{utmMedium}',
                utmCampaign   : '{utmCampaign}',
                utmTerm       : '{utmTerm}',
                utmContent    : '{utmContent}'
            }
        });
    });
}
// Integration End
// Counter Begin
$(document).ready(function() {
	(function(w, d, s, h, id) {
		w.roistatProjectId = id; w.roistatHost = h;
		var p = d.location.protocol == "https:" ? "https://" : "http://";
		var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
		var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
	})(window, document, 'script', 'cloud.roistat.com', '177a09b4dd887ac3e713c4606b6e9e30');
});
// Counter End
// Roistat End