$(document).ready(function(){
    $('.fancybox').fancybox();
    setTimeout(function(){
        $('#reviews').owlCarousel({
            items:1,
            dots:false,
            nav:true,
            loop:true,
            navSpeed:700
        });
    },300);
	$('input[name=phone],input[name=tel]').mask('+7 (999) 999-99-99');
    $('.tabs a').click(function(){
        var href = $(this).attr('href');
        $('.tabs a').removeClass('active');
        $(this).addClass('active');
        $('.tabs_content').hide();
        $(href).fadeIn(300);
        return false;
    });
	$('.home_partners').owlCarousel({

        dots:false,
        nav:true,
        loop:true,
        navSpeed:600,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				nav:true
			},
			600:{
				items:3
			},
			800:{
				items:5
			},
			1051:{
				items:5
			}
		}	
    });
    $('.top_menu a,footer nav a').click(function(){
        var id = $(this).attr('href');
        var top = $(id).offset().top;
        $('html,body').animate({scrollTop:top+'px'},500);
        return false;
    });
    /*$('.buttons a.skype').click(function(){
        $('.overlay,#skype').fadeIn(300);
        return false;
    });
    $('.header_contacts button,.block_9_right_content button').click(function(){
        $('.overlay,#zvonok').fadeIn(300);
        return false;
    
    $('.overlay,.closed').click(function(){
        $('.overlay,.popup_form').fadeOut(300);
    });});*/
    /*$('form').submit(function(e) {
        var f = $(this);
        e.preventDefault();
        var id = $(this).parent('div').attr('id');
        var title = $('input[name=title]',f).val();
        var send_email = $('input[name=send_email]',f).val();
        var url = location.href;
        var subject = $('input[name=form_message_skype]',f).val();
        if(id== 'skype') {
            var subject = $('input[name=form_message_skype]',f).val();
            var message = subject + ' ' + '"' + title + '"' +' ' + url+"<br>Логин Skype:"+$('input[name=skype]',f).val();
            console.log(message);
        }
        else if(id == 'zvonok') {
            var subject = $('input[name=form_message_zvonok]',f).val();
            var message = subject + ' ' + '"' + title + '"' +' ' + url+"<br>Телефон:"+$('input[name=phone]',f).val();
            console.log(message);
        }
        else if(id == 'consult') {
            var subject = 'Вопрос с формы обратной связи';
            var message = subject + ' ' + '"' + title + '"' +' ' + url+"<br>Телефон:"+$('input[name=phone]',f).val()+"<br>E-mail:"+$('input[name=email]',f).val()+"<br>Текст вопроса:"+$('textarea[name=message]',f).val();
            console.log(message);
        }
        else {
            var subject = 'Заявка с акционной формы обратной связи';
            var message = subject + ' ' + '"' + title + '"' +' ' + url+"<br>Телефон:"+$('input[name=phone]',f).val()+"<br>E-mail:"+$('input[name=email]',f).val();
            console.log(message);
        }
        var query = 'act=sender';
            query += '&send_email=' + encodeURIComponent(send_email);
            query += '&subject=' + encodeURIComponent(subject);
            query += '&message=' + encodeURIComponent(message);


        $.ajax({
            type: "POST",
            data: query,
            url: "./sender.php",
            dataType: "json",
            success: function(data) {
                if(data.result == 'ok') {
                    $('.overlay,#thanks').fadeIn(300);
                    
                    
                } else {
                    alert('Ошибка отправки! Пожалуйста, попробуйте позже.');
                }
            }
        });

     
        return false;
    });
*/
});

// Roistat Begin
// Calltracking Begin
$(document).ready(function() {
    // Header
    (function() {
        var phone = $('.phone_panel').find('p');

        phone.addClass('roistat-phone-tel');
        phone.html('<span>+<string class="roistat-phone-country">7</string> (<string class="roistat-phone-region">499</string>)</span> <string class="roistat-phone-number">703 03 03</string>');

    })();

    // Body
    (function() {
        var phone = $('.phones');

        phone.addClass('roistat-phone-tel');
        phone.html('+<string class="roistat-phone-country">7</string> (<string class="roistat-phone-region">499</string>) <span><string class="roistat-phone-number">703 03 03</string></span>');
    })();

    // Footer
    (function() {
        var phone = $('.footer_phone');

        phone.addClass('roistat-phone-tel');
        phone.html('+<string class="roistat-phone-country">7</string> (<string class="roistat-phone-region">499</string>) <span><string class="roistat-phone-number">703 03 03</string></span>');
    })();
});
// Calltracking End
// Counter Begin
$(document).ready(function() {
    (function(w, d, s, h, id) {
        w.roistatProjectId = id; w.roistatHost = h;
        var p = d.location.protocol == "https:" ? "https://" : "http://";
        var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
        var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
    })(window, document, 'script', 'cloud.roistat.com', '177a09b4dd887ac3e713c4606b6e9e30');
});
// Counter End
// Roistat End
