<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainArticle extends Model
{

    /**
     * Position
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\MainArticleType', 'type_id', 'id');
    }
}
