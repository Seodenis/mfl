<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassMediaArticle extends Model
{

    /**
     * Items by limit
     *
     * @param $limit
     *
     * @return mixed
     */
    public static function itemsByLimit($limit)
    {
        return MassMediaArticle::where('active', 1)->orderBy('id', 'DESC')->limit($limit)->get();
    }

    /**
     * Mass media
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mass_media()
    {
        return $this->belongsTo('App\MassMedia', 'mass_media_id', 'id');
    }
}
