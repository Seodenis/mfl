<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailTemplate extends Model
{
    /**
     * Item by const
     *
     * @param $const
     *
     * @return mixed
     */
    public static function getItemByConst($const)
    {
        return MailTemplate::whereConst($const)->first();
    }
}