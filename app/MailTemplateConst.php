<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailTemplateConst extends Model
{

    /**
     * Get template consts
     *
     * @param \App\MailTemplate $template
     *
     * @return mixed
     */
    public static function getConstsByTemplateId($id)
    {
        return MailTemplateConst::from('mail_template_consts AS mtc')
            ->select('mtc.*')
            ->join('mail_t_consts AS mc', 'mc.mail_template_const_id', '=', 'mtc.id')
            ->where('mc.mail_template_id', '=', $id)
            ->get();
    }
}