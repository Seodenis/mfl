<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $connection = 'pockethelp';

    /**
     * Get user requests by type
     *
     * @param \App\User $user
     * @param           $const
     *
     * @return string
     */
    public static function getItemsByConst(User $user, $const)
    {
        try {
            return Request::select('requests.*')
                ->join('request_types AS rt', 'requests.type_id', '=', 'rt.id')
                ->where(
                    [
                        'requests.owner_id' => $user->id,
                        'rt.const'          => strtoupper($const)
                    ]
                )
                ->where('requests.site_id', '!=', null)
                ->orderBy('requests.id', 'DESC')
                ->get();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Status
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo('App\RequestStatus', 'status_id', 'id');
    }

    /**
     * Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\RequestType', 'type_id', 'id');
    }
}
