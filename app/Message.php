<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $timestamps = false;
    protected $connection = 'pockethelp';
    protected $fillable = ['body'];

    protected $appends = ['selfMessage', 'userPath'];

    /**
     * Messages by request
     *
     * @param $requestId
     *
     * @return mixed
     */
    public static function getMessagesByRequest($requestId)
    {
        return Message::from('messages AS m')
            ->select(
                'm.bill_id',
                'm.from_user_id',
                'm.body',
                'm.attache',
                'm.attache_extention',
                'm.attache_size',
                'm.created_at'
            )
            ->whereRequestId($requestId)->orderBy('id', 'DESC')->get();
    }

    public function getSelfMessageAttribute()
    {
        return $this->from_user_id == auth()->user()->id;
    }

    public function getUserPathAttribute()
    {
        $path = user_folder_path(md5($this->from_user_id) . '/' . $this->attache);

        if (file_exists($path)) {
            return user_folder_front_path(md5($this->from_user_id) . '/' . $this->attache);
        } else {
            return env('APP_PH_URL') . user_folder_front_path(md5($this->from_user_id) . '/' . $this->attache);
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'from_user_id');
    }

    public function bill()
    {
        return $this->belongsTo('App\Bill', 'bill_id', 'id');
    }
}
