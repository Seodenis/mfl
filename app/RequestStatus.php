<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestStatus extends Model
{
    protected $connection = 'pockethelp';
    protected $table = 'request_status';
}
