<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WonCase extends Model
{

    /**
     * Specialization
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function specialization()
    {
        return $this->belongsTo('App\Specialization', 'specialization_id', 'id');
    }
}
