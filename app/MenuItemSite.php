<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItemSite extends Model
{
    public $timestamps = false;
}
