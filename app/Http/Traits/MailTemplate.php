<?php

namespace App\Http\Traits;

trait MailTemplate
{
    public $mailTemplate;
    public $mailTemplateConsts;

    /**
     * Template
     *
     * @param $const
     */
    protected function getTemplate($const)
    {
        $this->setConsts();
        $this->mailTemplate = \App\MailTemplate::getItemByConst($const);
    }

    private function setConsts()
    {
        $this->mailTemplateConsts = [
            '{-SITE_URL-}' => '<a href="' . config('app.url') . '">«' . setting('site.title') . '»</a>',
        ];
    }
}