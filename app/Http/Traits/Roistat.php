<?php


namespace App\Http\Traits;


use Illuminate\Http\Request;

trait Roistat
{
    protected $requestData;

    protected function addRoistatLead(Request $request)
    {

        // Данные должны быть в кодировке UTF-8! Иначе — это может привести к ошибке.
        // Если вы используете кодировку Windows-1251, то можно преобразовать все переменные через $value = iconv("Windows-1251", "UTF-8", $value);
        // или указать в доп. полях ключ 'charset' с используемой на сайте кодировкой, сервер Roistat, конвертирует все значения из указанной кодировки в UTF-8.

        // ...
        // Где-то здесь вызывается текущая функция создания сделки, например, функция mail().
        // ...

        $roistatData = array(
            'roistat'                                  => isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : 'no_cookie',
            'key'                                      => config('app.roistat_api_key'), // Ключ для интеграции с CRM, указывается в настройках интеграции с CRM.
            'title'                                    => 'Заявка с сайта "' .config('app.name') . '" №' . $request->request_id, // Название сделки
            'comment'                                  => $request->comment, // Комментарий к сделке
            'name'                                     => $request->name, // Имя клиента
            'email'                                    => $request->email, // Email клиента
            'phone'                                    => $request->phone, // Номер телефона клиента
            'order_creation_method'                    => '', // Способ создания сделки
            'is_need_callback'                         => '0', // После создания в Roistat заявки, Roistat инициирует обратный звонок на номер клиента, если значение параметра равно 1 и в Ловце лидов включен индикатор обратного звонка.
            'callback_phone'                           => '', // Переопределяет номер, указанный в настройках обратного звонка.
            'sync'                                     => '0', //
            'is_need_check_order_in_processing'        => '1', // Включение проверки заявок на дубли
            'is_need_check_order_in_processing_append' => '1', // Если создана дублирующая заявка, в нее будет добавлен комментарий об этом
            'fields'                                   => array(
                // Массив дополнительных полей. Если дополнительные поля не нужны, оставьте массив пустым.
                // Примеры дополнительных полей смотрите в таблице ниже.
                "SOURCE_ID" => $request->request_type,
                "type_treatment_info" => $request->request_messanger_contact,
                "current_page" => $request->request_url,
                "city" => $request->request_location ? json_decode($request->request_location)->cityName : null,
            ),
        );

        try {
            return file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));
        } catch (\Exception $exception) {

        }
    }
}
