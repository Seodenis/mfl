<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SeoController extends Controller
{
    /**
     * Get transliteration slug
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function slug(Request $request)
    {
        return response()->json(["result" => str_slug($request->text)]);
    }
}
