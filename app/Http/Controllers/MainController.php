<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Article;
use App\Employee;
use App\MainArticle;
use App\MassMediaArticle;
use App\News;
use App\Service;
use App\WonCase;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Midnite81\GeoLocation\Services\GeoLocation;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class MainController extends Controller
{

    public function test(GeoLocation $geo, Request $request)
    {
        dd($request->ip());
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('main');
    }

    /**
     * About
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        return view(
            'about',
            [
                'items' => $this->getMainArticles('ABOUT'),
            ]
        );
    }

    /**
     * Get main articles items
     *
     * @param $const
     *
     * @return mixed
     */
    private function getMainArticles($const)
    {
        return MainArticle::select('main_articles.*')
            ->where(
                [
                    'main_articles.site_id' => $this->site_id,
                    'main_articles.active'  => 1,
                    'mat.const'             => $const,
                ]
            )
            ->join('main_article_types AS mat', 'main_articles.type_id', '=', 'mat.id')
            ->get();
    }

    /**
     * Contacts
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contacts()
    {
        return view(
            'contacts',
            [
                'items' => $this->getMainArticles('CONTACTS'),
            ]
        );
    }

    /**
     * Work
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function work()
    {
        return view(
            'work',
            [
                'items' => $this->getMainArticles('WORK'),
            ]
        );
    }

    /**
     * Payment
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function payment()
    {
        return view(
            'payment',
            [
                'items' => $this->getMainArticles('PAYMENT'),
            ]
        );
    }

    /**
     * Press center
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pressCenter()
    {
        return view(
            'press-center',
            [
                'items'     => $this->getMainArticles('PRESS-CENTER'),
                'massMedia' => MassMediaArticle::itemsByLimit(3)
            ]
        );
    }

    /**
     * Privilege
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function privilege()
    {
        return view(
            'privilege',
            [
                'items' => $this->getMainArticles('PRIVILEGE'),
            ]
        );
    }

    /**
     * Sitemap
     */
    public function sitemap()
    {
        $sitemap = Sitemap::create();
        foreach ($this->getSitemapData() as $k => $v) {
            $sitemap->add(
                Url::create('/' . $v['uri'])
                    ->setLastModificationDate(Carbon::createFromTimeString($v['updated_at']))
                    ->setChangeFrequency(false)
                    ->setPriority(false)
            );
        }

        $file = base_path('public/storage/files/sitemap.xml');

        File::isFile($file) or fopen($file, "w+");

        $sitemap->writeToFile($file);

        return response(
            File::get($file),
            200,
            [
                'Content-Type' => 'application/xml',
            ]
        );
    }

    /**
     * Merge all content for sitemap
     *
     * @return array
     */
    private function getSitemapData()
    {
        $result = Answer::select('uri', 'updated_at')->where('active', '=', 1)->where('site_id', '=', static::$siteId)->get()->toArray();
        $result = array_merge($result, Article::select('uri', 'updated_at')->where('active', '=', 1)->where('site_id', '=', static::$siteId)->get()->toArray());
        $result = array_merge($result, Employee::select('uri', 'updated_at')->where('active', '=', 1)->where('site_id', '=', static::$siteId)->get()->toArray());
        $result = array_merge($result, MainArticle::select('uri', 'updated_at')->where('active', '=', 1)->where('site_id', '=', static::$siteId)->get()->toArray());
        $result = array_merge($result, MassMediaArticle::select('uri', 'updated_at')->where('active', '=', 1)->where('site_id', '=', static::$siteId)->get()->toArray());
        $result = array_merge($result, News::select('uri', 'updated_at')->where('active', '=', 1)->where('site_id', '=', static::$siteId)->get()->toArray());
        $result = array_merge($result, Service::select('uri', 'updated_at')->where('active', '=', 1)->where('site_id', '=', static::$siteId)->get()->toArray());
        $result = array_merge($result, WonCase::select('uri', 'updated_at')->where('active', '=', 1)->where('site_id', '=', static::$siteId)->get()->toArray());

        return collect($result)->unique('uri')->toArray();
    }

    /**
     * Get single main article by slug
     *
     * @param $slug
     *
     * @return mixed
     */
    public function mainArticle(Request $request, $slug)
    {
        return view(
            'main-article',
            [
                'item' => $this->itemMainArticle($slug),
            ]
        );
    }

    private function itemMainArticle($slug)
    {
        return MainArticle::whereSlug($slug)
            ->where('site_id', $this->site_id)
            ->first();
    }
}
