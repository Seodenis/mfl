<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventType;
use App\Mail\NotificationMail;
use App\Message;
use App\Request as Requests;
use App\User;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class MessageController extends Controller
{
    /**
     * My unread items by request id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function unreadItemsByRequestId($requestId)
    {
        return Message::where('messages.from_user_id', '!=', Auth::user()->id)
            ->where(
                function ($query) {
                    $query->where('requests.owner_id', '=', Auth::user()->id)->orWhere('requests.lawyer_id', '=', Auth::user()->id);
                }
            )
            ->where(['read' => 0, 'request_id' => $requestId])
            ->join('requests', 'requests.id', '=', 'messages.request_id')
            ->count();
    }

    public function index($requestId)
    {
        $messages = Message::with(['user', 'bill'])->where(['request_id' => $requestId])->get();

        return response()->json($messages);
    }

    /**
     * My unread items
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function unreadItems()
    {
        $messages = Message::where('messages.from_user_id', '!=', Auth::user()->id)
            ->where(
                function ($query) {
                    $query->where('requests.owner_id', '=', Auth::user()->id)->orWhere('requests.lawyer_id', '=', Auth::user()->id);
                }
            )
            ->where(['read' => 0])
            ->join('requests', 'requests.id', '=', 'messages.request_id')
            ->count();

        return response()->json($messages);
    }

    /**
     * My unread changes
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function unreadChanges()
    {
        $messages = Event::where(['events.read' => null])
            ->selectRaw('IF(ISNULL(uns.push), et.push, uns.push) AS push')
            ->where(['et.alert' => 1])
            ->where(['events.user_id' => Auth::user()->id])
            ->join('event_types AS et', 'events.type_id', '=', 'et.id')
            ->leftJoin(
                'user_notification_settings AS uns',
                function ($join) {
                    $join->on('et.id', '=', 'uns.type_id');
                    $join->where('uns.user_id', Auth::user()->id);
                }
            )
            ->having('push', 1)
            ->get();

        return response()->json($messages->count());
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $message = $request->user()->messages()->create(
            [
                'body' => $request->body,
            ]
        );
        broadcast(new MessageCreated($message))
            ->toOthers();

        return response()->json($message);
    }

    /**
     * @author ansotov
     *
     * @param Request $request
     */
    public function add(Request $request)
    {
        self::updatedRead($request->requestId);

        $validator = Validator::make(
            $request->all(),
            [
                'files' => 'mimes:jpeg,png,jpg,gif,svg,doc,docx,pdf,rtf,txt,rar,zip,xls,xlsx|max:31000',
            ]
        );

        if ($validator->fails() && $request->body == false) {
            echo json_encode(['error' => 'Документ может быть только изображением или текстовым документом, не более 30Мб']);
            exit;
        }

        $requestItem         = \App\Request::where(['id' => $request->requestId])->first();
        $requestItem->closed = 0;
        $requestItem->save();

        $message               = new Message();
        $message->request_id   = $request->requestId;
        $message->body         = $request->body;
        $message->from_user_id = Auth::id();

        // Attached file
        if ($request->file('files')) {
            $data = $request->file('files');

            $this->setFolder();

            try {
                $file = new File($data->getPathname());

                $extention = $data->extension();

                $originalName = substr($data->getClientOriginalName(), 0, strrpos($data->getClientOriginalName(), "."));

                if ($validator->fails()) {
                    echo json_encode(['error' => 'Документ может быть только изображением или текстовым документом, не более 30Мб']);
                    exit;
                }

                $fileName = $originalName . '-' . md5($message->id . time()) . '.' . $extention;

                $file->move($this->userFolder, $fileName);

                if (is_image($data->getClientMimeType()) == true) {
                    $finalFileName = $originalName . '-' . md5($message->id . (time() + 1)) . '.' . $extention;
                    Image::make($this->userFolder . '/' . $fileName)->widen(1000)->save($this->userFolder . '/' . $finalFileName);

                    $size = get_filesize($this->userFolder . '/' . $finalFileName);
                } else {
                    $size = get_filesize($this->userFolder . '/' . $fileName);
                }

                //$updateMessage                    = Message::whereId($message->id)->first();
                $message->attache           = $fileName;
                $message->attache_extention = $extention;
                $message->attache_size      = $size;

                $requestItem = Requests::whereId($request->requestId)->first();

                // Event
                if ($requestItem->owner_id != $message->from_user_id) {
                    EventController::save(EventType::where(['const' => 'FILE_FROM_LAWYER'])->first(), $fileName, $requestItem->owner_id, $requestItem->id);
                }
            } catch (\Exception $exception) {
                echo $exception->getMessage();
            }

        }

        $message->save();
    }

    /**
     * Update read by request id
     *
     * @param $requestId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function updatedRead($requestId)
    {
        $messages = Message::where('messages.from_user_id', '!=', Auth::user()->id)
            ->where(
                function ($query) {
                    $query->where('requests.owner_id', '=', Auth::user()->id)->orWhere('requests.lawyer_id', '=', Auth::user()->id);
                }
            )
            ->where(['messages.read' => 0, 'requests.id' => $requestId])
            ->join('requests', 'requests.id', '=', 'messages.request_id')
            ->update(['messages.read' => 1]);

        return response()->json($messages);
    }

}
