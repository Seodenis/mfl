<?php

namespace App\Http\Controllers;

use App\Specialization;
use App\SpecializationItem;
use Illuminate\Http\Request;

class SpecializationController extends Controller
{

    /**
     * Specializations
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view(
            'specializations.index',
            [
                'items' => $this->getSpecializations(),
            ]
        );
    }

    /**
     * Specializations data
     *
     * @return mixed
     */
    private function getSpecializations()
    {
        return Specialization::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
            ]
        )
            ->orderBy('order', 'ASC')
            /*->limit($this->limit)
            ->offset($this->offset)
            ->paginate();*/
            ->get();
    }

    /**
     * Specialization
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function item(Request $request)
    {
        return view(
            'specializations.view',
            [
                'item'      => $this->getItem($request->slug),
                'dataItems' => $this->getDataItems($request->slug),
            ]
        );
    }

    /**
     * Specialization data
     *
     * @param $slug
     *
     * @return mixed
     */
    private function getItem($slug)
    {
        return Specialization::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
                'slug'    => $slug,
            ]
        )->first();
    }

    /**
     * Specialization data items
     *
     * @param $slug
     *
     * @return mixed
     */
    private function getDataItems($slug)
    {
        $category = Specialization::where(['slug' => $slug])->first();

        return SpecializationItem::where(
            [
                'active'    => 1,
                'site_id'   => $this->site_id,
                'parent_id' => $category->id,
            ]
        )
            ->orderBy('order', 'ASC')
            ->get();
    }

    public function dataItem($catSlug, $slug)
    {
        return view(
            'specializations.items.view',
            [
                'item' => $this->getDataItem($catSlug, $slug),
            ]
        );
    }

    /**
     * Specialization data item
     *
     * @param $slug
     *
     * @return mixed
     */
    private function getDataItem($catSlug, $slug)
    {
        $category = Specialization::where(['slug' => $catSlug])->first();

        return SpecializationItem::where(
            [
                'active'    => 1,
                'site_id'   => $this->site_id,
                'parent_id' => $category->id,
                'slug'      => $slug,
            ]
        )->first();
    }
}
