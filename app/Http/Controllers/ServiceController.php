<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    /**
     * Services
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view(
            'services.index',
            [
                'items' => $this->getServices(),
            ]
        );
    }

    /**
     * Hot services
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function hot(Request $request)
    {
        return view(
            'services.hot.index',
            [
                'items' => $this->getServices(1),
            ]
        );
    }

    /**
     * Services data
     *
     * @param int $hot
     *
     * @return mixed
     */
    private function getServices($hot = 0)
    {
        return Service::where(
            [
                'hot'     => $hot,
                'active'  => 1,
                'site_id' => $this->site_id,
            ]
        )
            ->orderBy('order', 'ASC')
            ->limit($this->limit)
            ->offset($this->offset)
            ->paginate();
    }

    /**
     * Service
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function item(Request $request)
    {
        return view(
            'services.view',
            [
                'item' => $this->getService($request->slug),
            ]
        );
    }

    /**
     * Service hot item
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function hotItem(Request $request)
    {
        return view(
            'services.hot.view',
            [
                'item' => $this->getService($request->slug),
            ]
        );
    }

    /**
     * Service data
     *
     * @param $slug
     *
     * @return mixed
     */
    private function getService($slug)
    {
        return Service::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
                'slug'    => $slug,
            ]
        )->first();
    }
}
