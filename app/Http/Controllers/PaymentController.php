<?php

namespace App\Http\Controllers;

use App\Bill;
use App\BillType;
use App\EventType;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function check(Request $request)
    {

        $secret_seed = config('app.paykeeper_secret_seed');
        $id          = $request->get('id');
        $sum         = $request->get('sum');
        $clientid    = $request->get('clientid');
        $orderid     = $request->get('orderid');
        $key         = $request->get('key');

        $clientEmail = $request->get('client_email');

        if ($user = User::where(['email' => $clientEmail, 'blocked' => 0])->first()) {
            if ($key != md5(
                    $id . number_format($sum, 2, ".", "")
                    . $clientid . $orderid . $secret_seed
                )) {
                echo "Error! Hash mismatch";
                exit;
            } else {
                DB::beginTransaction();

                $bill       = Bill::whereId($orderid)->first();

                if ($bill->sum != $sum) {
                    DB::rollBack();
                    echo "Error! Hash mismatch";
                    exit;
                }

                $bill->transaction_id = $id;
                $bill->paid = true;
                $bill->save();

                try {

                    EventController::save(EventType::where(['const' => 'BILL_PAID'])->first(), 'Тразакция №' . $id, $bill->user->id);

                    DB::commit();
                } catch (\Exception $exception) {
                    DB::rollBack();
                    echo "Error! Hash mismatch";
                    exit;
                }
            }
        } else {
            echo "Error! Hash mismatch";
            exit;
        }


        echo "OK " . md5($id . $secret_seed);
        exit();
    }
}
