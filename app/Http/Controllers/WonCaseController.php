<?php

namespace App\Http\Controllers;

use App\WonCase;
use Illuminate\Http\Request;

class WonCaseController extends Controller
{

    /**
     * Cases
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view(
            'won_cases.index',
            [
                'items' => $this->getCases(),
            ]
        );
    }

    /**
     * Cases data
     *
     * @return mixed
     */
    private function getCases()
    {
        return WonCase::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
            ]
        )
            ->orderBy('created_at', 'DESC')
            ->limit($this->limit)
            ->offset($this->offset)
            ->paginate();
    }

    /**
     * Case
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function item(Request $request)
    {
        return view(
            'won_cases.view',
            [
                'item' => $this->getCase($request->slug),
            ]
        );
    }

    /**
     * Case data
     *
     * @param $slug
     *
     * @return mixed
     */
    private function getCase($slug)
    {
        return WonCase::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
                'slug'    => $slug,
            ]
        )->first();
    }
}
