<?php

namespace App\Http\Controllers;

use App\Bill;
use App\EventType;
use App\Http\Traits\MailTemplate;
use App\Mail\NotificationMail;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class BillController extends Controller
{
    use MailTemplate;

    /**
     * @param Request $request
     *
     * @return string
     */
    public function toPay(Request $request)
    {
        if ($request->save) {

            if ($request->bill_id) {
                DB::beginTransaction();

                try {
                    $this->item = Bill::where(['id' => $request->bill_id, 'user_id' => Auth::user()->id])->first();

                    $this->item->paid = 1;

                    $this->item->save();

                    $user = Auth::user();

                    $user->score = ($user->score - $this->item->sum);

                    $user->save();

                    $request->sum = $this->item->sum;

                    $message               = new Message();
                    $message->request_id   = $this->item->request_id;
                    $message->body         = 'Счет №' . $this->item->id . ' оплачен';
                    $message->from_user_id = Auth::id();
                    $message->save();

                    // Mail template data
                    $this->getTemplate('BILL_LAWYER_PAID');

                    array_set($this->mailTemplateConsts, '{-NUM-}', $request->sum);
                    array_set($this->mailTemplateConsts, '{-SUM-}', $request->sum);
                    array_set($this->mailTemplateConsts, '{-LINK-}', '<a href="' . route('requests.show', ['id' => $this->item->request_id]) . '">' . route('requests.show', ['id' => $this->item->request_id]) . '</a>');

                    $this->mailTemplate->message = replaceData($this->mailTemplateConsts, $this->mailTemplate->message);

                    $requestItem = \App\Request::whereId($this->item->request_id)->first();

                    // Send lawyer email
                    Mail::to($requestItem->lawyer->email)
                        ->send(new NotificationMail($requestItem->lawyer, $this->mailTemplate->subject, $this->mailTemplate->message));

                    DB::commit();
                } catch (\Exception $exception) {
                    DB::rollback();

                    return $exception->getMessage();
                }
            }

            // Event
            EventController::save(EventType::where(['const' => 'BILL_PAID'])->first(), '№' . $this->item->id);

            // Mail template data
            $this->getTemplate('BILL_PAID');

            array_set($this->mailTemplateConsts, '{-NUM-}', $request->sum);
            array_set($this->mailTemplateConsts, '{-SUM-}', $request->sum);
            array_set($this->mailTemplateConsts, '{-LINK-}', '<a href="' . route('requests.show', ['id' => $this->item->request_id]) . '">' . route('requests.show', ['id' => $this->item->request_id]) . '</a>');

            $this->mailTemplate->message = replaceData($this->mailTemplateConsts, $this->mailTemplate->message);

            // Send email
            Mail::to(Auth::user()->email)
                ->send(new NotificationMail(Auth::user(), $this->mailTemplate->subject, $this->mailTemplate->message));


        } else {
            if ($request->bill_id) {
                $this->item          = Bill::whereId($request->bill_id)->first();
                $this->item->payType = 'bill';
            }

            return view('bills.partials.toPay')
                ->with(
                    [
                        'item' => $this->item,
                    ]
                );
        }
    }
}
