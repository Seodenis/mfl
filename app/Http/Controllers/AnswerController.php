<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Http\Request;

class AnswerController extends Controller
{

    /**
     * Answers
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view(
            'answers.index',
            [
                'items' => $this->getAnswers(),
            ]
        );
    }

    /**
     * Answers data
     *
     * @return mixed
     */
    private function getAnswers()
    {
        return Answer::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
            ]
        )
            ->orderBy('created_at', 'DESC')
            ->limit($this->limit)
            ->offset($this->offset)
            ->paginate();
    }

    /**
     * Answer
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function item(Request $request)
    {
        return view(
            'answers.view',
            [
                'item' => $this->getAnswer($request->slug),
            ]
        );
    }

    /**
     * Answer data
     *
     * @param $slug
     *
     * @return mixed
     */
    private function getAnswer($slug)
    {
        return Answer::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
                'slug'    => $slug,
            ]
        )->first();
    }
}
