<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class AboutController extends Controller
{

    /**
     * Employees
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function employees(Request $request)
    {
        return view(
            'about.employees.index',
            [
                'items' => $this->getEmployees(),
            ]
        );
    }

    /**
     * Employees data
     *
     * @return mixed
     */
    private function getEmployees()
    {
        return Employee::where(
            [
                'active' => 1,
            ]
        )
            ->where(
                function ($query) {
                    $query->where('site_id', $this->site_id)->orWhere('site_id', null);
                }
            )
            ->orderBy('order', 'ASC')
            ->limit($this->limit)
            ->offset($this->offset)
            ->paginate();
    }

    /**
     * Employee
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function itemEmployee(Request $request)
    {
        return view(
            'about.employees.view',
            [
                'item' => $this->getEmployee($request->slug),
            ]
        );
    }

    /**
     * Employee data
     *
     * @param $slug
     *
     * @return mixed
     */
    private function getEmployee($slug)
    {
        return Employee::where(
            [
                'active'  => 1,
                'slug'    => $slug,
            ]
        )->first();
    }
}
