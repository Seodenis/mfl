<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleCat;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    private $category;

    /**
     * All articles
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view(
            'articles.index',
            [
                'items'      => $this->getArticles(),
                'categories' => $this->getCategories(),
            ]
        );
    }

    /**
     * Articles by category
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category(Request $request)
    {
        $this->category = ArticleCat::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
                'slug'    => $request->catSlug,
            ]
        )->first();

        return view(
            'articles.category',
            [
                'items'      => $this->getArticlesByCat(),
                'category' => ArticleCat::where(['slug' => $request->catSlug])->first(),
                'categories' => $this->getCategories(),
            ]
        );
    }

    /**
     * Articles by category
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function item(Request $request)
    {
        return view(
            'articles.view',
            [
                'item'      => $this->getArticle($request->slug),
                'categories' => $this->getCategories(),
            ]
        );
    }

    /**
     * Article
     *
     * @param $slug
     *
     * @return mixed
     */
    private function getArticle($slug)
    {
        return Article::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
                'slug'    => $slug,
            ]
        )->first();
    }

    /**
     * Articles
     *
     * @return mixed
     */
    private function getArticles()
    {
        return Article::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
            ]
        )
            ->orderBy('created_at', 'DESC')
            ->limit($this->limit)
            ->offset($this->offset)
            ->paginate();
    }

    /**
     * Articles by category
     *
     * @return mixed
     */
    private function getArticlesByCat()
    {
        return Article::where(
            [
                'active'    => 1,
                'site_id'   => $this->site_id,
                'parent_id' => $this->category->id,
            ]
        )
            ->orderBy('created_at', 'DESC')
            ->limit($this->limit)
            ->offset($this->offset)
            ->paginate();
    }

    /**
     * Categories
     *
     * @return mixed
     */
    private function getCategories()
    {
        return ArticleCat::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
            ]
        )
            ->orderBy('order', 'ASC')
            ->get();
    }
}
