<?php

namespace App\Http\Controllers;

use App\EventType;
use App\Http\Traits\MailTemplate;
use App\Mail\NotificationMail;
use App\Message;
use App\Request as RequestModel;
use App\RequestStatus;
use App\RequestType;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class RequestsController extends Controller
{
    private $paid = 1;

    use MailTemplate;

    public function __construct()
    {
        parent::__construct();
    }

    public static function deleteOldEmptyItems()
    {
        $items = \App\Request::oldEmptyItems();
        $items->remove();

        dd($items);
    }

    /**
     * Delete empty and old requests
     */
    public static function deleteEmptyOldRequests()
    {
        foreach (\App\Request::oldEmptyItems() as $k => $v) {
            $v->delete();
        }
    }

    /**
     * Set empty lawyer for don`t answered requests
     */
    public function setEmptyRequests()
    {
        foreach (\App\Request::notAnsweredItems() as $k => $v) {

            // Send message
            $user = User::whereId($v->lawyer_id)->first();

            $this->getTemplate('NOT_UNSWERED_REQUEST');

            array_set($this->mailTemplateConsts, '{-NUM-}', $v->id);
            array_set($this->mailTemplateConsts, '{-LINK-}', '<a href="' . route('requests') . '">' . route('requests') . '</a>');

            $this->mailTemplate->message = replaceData($this->mailTemplateConsts, $this->mailTemplate->message);

            Mail::to($user->email)->send(new NotificationMail($user, $this->mailTemplate->subject, $this->mailTemplate->message));

            $v->lawyer_id = null;
            $v->save();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::whereId(Auth::user()->role_id)->first();

        if (Auth::user()->role->name == 'lawyer') {
            $this->items = \App\Request::lawyerRequests(Auth::user());
        } else {
            $this->items = \App\Request::whereOwnerId(Auth::id())->where('site_id', null)->orderBy('id', 'desc')->get();
        }

        $view = ($role->name == 'lawyer') ? 'requests.lawyer.index' : 'requests.index';

        // If user haven`t requests create first
        if ($this->items->count() == 0 && Auth::user()->role->name != 'lawyer') {
            return Redirect::route('requests.create');
        }

        return view($view)
            ->with(
                [
                    'items' => $this->items,
                ]
            );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user(Request $request, $id)
    {
        $this->items = \App\Request::whereOwnerId($id)->orderBy('id', 'desc')->get();

        // If user haven`t requests create first
        if ($this->items->count() == 0) {
            return Redirect::route('main');
        }

        return view('requests.user.index')
            ->with(
                [
                    'items' => $this->items,
                    'user'  => User::whereId($id)->first(),
                ]
            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->item           = new \App\Request();
        $this->item->owner_id = Auth::id();
        $this->item->type_id  = RequestType::where(['const' => 'CHAT'])->first()->id;
        $this->item->save();

        // Set lawyer
        //$this->setLawyer($this->item);

        // Event
        EventController::save(EventType::where(['const' => 'REQUEST_LAWYER'])->first(), false);

        return redirect(route('requests.show', ['id' => $this->item->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Update read messages
        MessageController::updatedRead($id);

        $role = Role::whereId(Auth::user()->role_id)->first();

        $this->item = \App\Request::whereId($id)->first();

        $view = ($role->name == 'lawyer') ? 'requests.lawyer.show' : 'requests.show';

        foreach ($this->item->bills as $k => $v) {
            if ($v->paid == 0) {
                $this->paid = 0;
            }
        }

        return view($view)
            ->with(
                [
                    'item' => $this->item,
                    'paid' => $this->paid,
                ]
            );
    }

    /**
     * Item
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getItem($id)
    {
        return response()->json(\App\Request::whereId($id)->first());
    }

    /**
     * Get chat
     *
     * @author ansotov
     *
     * @param $requestId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function chat($requestId)
    {
        $messages = Message::with(['user'])->where(['request_id' => $requestId])->get();
        $request  = RequestModel::whereId($requestId)->first();

        return view('requests.lawyer.chat')
            ->with(
                [
                    'request'  => $request,
                    'messages' => $messages,
                ]
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            RequestModel::where(['id' => $id, 'owner_id' => Auth::user()->id])->delete();

            return redirect()->route('requests');
        } catch (\Exception $exception) {
            return redirect()->back()
                ->withErrors(['error' => $exception->getMessage()]);
        }
    }

    /**
     * Close request
     *
     * @param $id
     *
     * @return string
     */
    public function close($id)
    {
        try {

            $status = RequestStatus::whereConst('CLOSED')->first();

            $this->item = \App\Request::where(['id' => $id, 'closed' => 0])->first();

            if ($status) {
                $this->item->status_id = $status->id;
            }

            $this->item->closed = 1;
            $this->item->save();

            $user = User::whereId($this->item->owner_id)->first();
            // Event
            EventController::save(EventType::where(['const' => 'REQUEST_LAWYER_CLOSE'])->first(), 'обращение №' . $this->item->id, $user->id, $this->item->id);

            // Mail template data
            $this->getTemplate('REQUEST_CLOSED');

            array_set($this->mailTemplateConsts, '{-NUM-}', $this->item->id);
            array_set($this->mailTemplateConsts, '{-LINK-}', '<a href="' . route('requests.show', ['id' => $this->item->id]) . '">' . route('requests.show', ['id' => $this->item->id]) . '</a>');

            $this->mailTemplate->message = replaceData($this->mailTemplateConsts, $this->mailTemplate->message);

            Mail::to($user->email)->send(new NotificationMail($user, $this->mailTemplate->subject, $this->mailTemplate->message));

            $this->getTemplate('REQUEST_LAWYER_RATE');

            array_set($this->mailTemplateConsts, '{-LINK-}', '<a href="' . route('requests.show', ['id' => $this->item->id]) . '">' . route('requests.show', ['id' => $this->item->id]) . '</a>');

            $this->mailTemplate->message = replaceData($this->mailTemplateConsts, $this->mailTemplate->message);

            Mail::to($user->email)->send(new NotificationMail($user, $this->mailTemplate->subject, $this->mailTemplate->message));
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Accept
     *
     * @param $id
     *
     * @return bool
     */
    public function accept($id)
    {
        if (Auth::check() && Auth::user()->role->name == 'lawyer') {
            $this->item            = \App\Request::whereId($id)->first();
            $this->item->lawyer_id = Auth::user()->id;

            $this->item->save();

            return back()->with('message', 'Вы успешно приняли заявку.');
        } else {
            return false;
        }
    }

    private function setLawyer(\App\Request $item)
    {
        $lawyer = $this->getEmptyLawyer();

        $item->lawyer_id = $lawyer->id;
        $item->save();

        // Mail template data
        $this->getTemplate('REQUEST_NEW_LAWYER');

        array_set($this->mailTemplateConsts, '{-NUM-}', $item->id);
        array_set($this->mailTemplateConsts, '{-LINK-}', '<a href="' . route('requests.show', ['id' => $item->id]) . '">' . route('requests.show', ['id' => $item->id]) . '</a>');

        $this->mailTemplate->message = replaceData($this->mailTemplateConsts, $this->mailTemplate->message);

        Mail::to($lawyer->email)
            ->send(new NotificationMail($lawyer, $this->mailTemplate->subject, $this->mailTemplate->message, false));
    }

    private function getEmptyLawyer()
    {
        return User::from('users AS u')
            ->select('u.id', 'u.email', 'u.name')
            ->selectRaw('(SELECT count(id) FROM requests WHERE lawyer_id = u.id AND closed = 0) as count_requests')
            ->join('roles AS r', 'r.id', '=', 'u.role_id')
            ->where('r.name', '=', 'lawyer')
            ->groupBy('u.id')
            ->orderBy('count_requests')
            ->first();
    }
}
