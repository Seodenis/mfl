<?php

namespace App\Http\Controllers;

use App\EventType;
use App\Http\Traits\Roistat;
use App\Mail\NotificationMail;
use App\RequestType;
use App\RequestValue;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Midnite81\GeoLocation\Services\GeoLocation;

class RequestController extends Controller
{
    use Roistat;

    private $type;

    /**
     * Add item
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Request
     */
    public function addItem(Request $request, GeoLocation $geo, $type = 'PHONE_CALL')
    {
        if ($request->save) {

            $type = $request->type ? strtoupper($request->type) : strtoupper($type);

            // Check type
            $this->checkType($type);

            DB::beginTransaction();

            try {
                $this->item = new \App\Request();

                $this->item->site_id   = $this->site_id;
                $this->item->type_id   = $this->type->id;
                $this->item->owner_id  = Auth::check() ? Auth::user()->id : null;
                $this->item->lawyer_id = $this->setLawyer($this->item);

                $this->item->save();

                // Lead data
                $request->merge(['request_id' => $this->item->id]);
                $request->merge(['comment' => null]);
                $request->merge(['request_messanger_contact' => null]);
                $request->merge(['request_url' => null]);
                $request->merge(['request_location' => null]);

                if (auth()->check()) {
                    $request->merge(['name' => auth()->user()->name]);
                    $request->merge(['email' => auth()->user()->email]);
                    $request->merge(['phone' => auth()->user()->phone]);
                } else {
                    $request->merge(['name' => null]);
                    $request->merge(['email' => null]);
                    $request->merge(['phone' => 'CHAT_' . $this->item->id]);
                }

                $ipLocation = $geo->getCity($request->ip());

                $request->merge(['request_type' => $this->item->type->const]);

                $message = $request->message ? $request->message : null;
                $currentUrl = $request->url ? $request->url : $request->url();
                $location = $ipLocation ? $ipLocation->toJson() : null;

                $request->merge(['comment' => $message]);
                $request->merge(['request_url' => $currentUrl]);
                $request->merge(['request_location' => $location]);

                if ($this->item->type->const != 'SKYPE_CALL') {
                    $request->merge(['phone' => $request->contact_info]);
                } else {
                    $request->merge(['phone' => null]);
                    $request->merge(['request_messanger_contact' => $request->contact_info]);
                }

                if ( ! auth()->check()) {
                    $request->merge(['name' => 'Заявка от ' . $request->contact_info]);
                }

                // If is not chat set values
                if ($this->type->const != 'CHAT') {

                    $value = new RequestValue();

                    $value->request_id   = $this->item->id;
                    $value->contact_info = $request->contact_info;
                    $value->message      = $message;
                    $value->url          = $currentUrl;
                    $value->location     = $location;

                    $value->save();
                }

                // Event
                if (Auth::check()) {
                    EventController::save(EventType::where(['const' => 'REQUEST_LAWYER'])->first(), false);
                }

                $this->addRoistatLead($request);

                DB::commit();

                if (request()->ajax()) {
                    return response()->json(['success' => 'Заявка отправлена, мы с Вами скоро свяжемся.']);
                } else {
                    if ($type == 'CHAT') {
                        return redirect()->route('profile.requests.item', ['id' => $this->item->id])->with('success', 'Ваша заявка создана – можете написать юристу сообщение в чате');
                    } else {
                        return redirect()->back()->with('success', 'Заявка отправлена, мы с Вами скоро свяжемся.');
                    }
                }
            } catch (\Exception $exception) {
                DB::rollback();

                return $exception->getMessage();
            }

        }

        if (request()->ajax()) {
            return view('requests.ajax.add', ['type' => $type]);
        } else {
            return view('requests.add', ['type' => $type]);
        }
    }

    /**
     * Check type
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    private function checkType($type)
    {
        $this->type = RequestType::where('const', strtoupper($type))->first();

        if ($this->type) {
            if ($this->type->const == 'CHAT' && ! Auth::check()) {
                return redirect()->back()->with('error', 'Нужно авторизоваться');
            }
        } else {
            return redirect()->back()->with('error', 'Нет такого типа заявки.');
        }
    }

    /**
     * Set lawyer
     *
     * @param \App\Request $item
     *
     * @return mixed
     */
    private function setLawyer(\App\Request $item)
    {
        return null;

        $lawyer = User::getEmptyLawyer();

        $item->lawyer_id = $lawyer->id;
        $item->save();

        // Send email message to lawyer about new request
        $text = '<p>Вам назначена новая заявка №' . $item->id . '</p><p>Для того, чтобы начать общение перейдите по ссылке ' . route('requests.show', ['id' => $item->id]) . '</p>';

        Mail::to($lawyer->email)
            ->send(new NotificationMail($lawyer, 'Новая заявка с сайта ' . $this->site->url, $text, false));

        return $item->lawyer_id;
    }

    /**
     * Item
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getItem($id)
    {
        return response()->json(\App\Request::whereId($id)->first());
    }
}
