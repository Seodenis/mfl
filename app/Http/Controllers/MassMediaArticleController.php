<?php

namespace App\Http\Controllers;

use App\MassMediaArticle;
use Illuminate\Http\Request;

class MassMediaArticleController extends Controller
{

    /**
     * Mass media articles
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view(
            'mass_media_articles.index',
            [
                'items' => $this->getArticles(),
            ]
        );
    }

    /**
     * Mass media articles data
     *
     * @return mixed
     */
    private function getArticles()
    {
        return MassMediaArticle::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
            ]
        )
            ->orderBy('created_at', 'DESC')
            ->limit($this->limit)
            ->offset($this->offset)
            ->paginate();
    }

    /**
     * Mass media article
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function item(Request $request)
    {
        return view(
            'mass_media_articles.view',
            [
                'item' => $this->getArticle($request->slug),
            ]
        );
    }

    /**
     * Mass media article data
     *
     * @param $slug
     *
     * @return mixed
     */
    private function getArticle($slug)
    {
        return MassMediaArticle::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
                'slug'    => $slug,
            ]
        )->first();
    }
}
