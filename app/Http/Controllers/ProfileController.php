<?php

namespace App\Http\Controllers;

use App\EventType;
use App\Mail\NotificationMail;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ProfileController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('profile/index')
            ->with(
                [
                    'items' => $this->items,
                ]
            );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function requests()
    {

        return view('profile/requests/index')
            ->with(
                [
                    'items' => \App\Request::getItemsByConst(Auth::user(), 'chat'),
                ]
            );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function requestItem(Request $request, $id)
    {
        if ($item = \App\Request::whereId($id)->whereOwnerId(Auth::user()->id)->first()) {
            return view('profile/requests/item')
                ->with(
                    [
                        'item' => $item,
                    ]
                );
        } else {
            return redirect(route('main'));
        }
    }

    /**
     * Settings
     *
     * @author ansotov
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function settings(Request $request)
    {

        if ($request->get('save')) {
            $user = Auth::user();

            if ($request->get('password')) {
                if ($request->get('password') == $request->get('re_password')) {
                    $user->password = Hash::make($request->get('password'));
                } else {
                    $this->exception = 'Пароли не совпадают';
                }
            } else {
                $user->name      = $request->get('name');
                $user->phone     = $request->get('phone');
                $user->email     = $request->get('email');
                $user->birthdate = Carbon::parse($request->get('birthdate'))->format('Y-m-d');
                $user->city      = $request->get('city');
            }

            if ($this->exception == false) {
                if ($user->save()) {

                    if ($request->get('password')) {
                        Mail::to(auth()->user()->email)
                            ->send(
                                new NotificationMail(
                                    Auth::user(),
                                    'Смена пароля',
                                    'Из личного кабинета пользователя на сайте ' . config('app.url') . ' произведена смена пароля. Если вы не меняли пароль, пожалуйста, сообщите нам об этом в ответном письме.'
                                )
                            );

                        // Event
                        EventController::save(EventType::where(['const' => 'PROFILE_PASSWORD_EDITED'])->first(), false);

                    } else {
                        Mail::to(auth()->user()->email)
                            ->send(
                                new NotificationMail(
                                    Auth::user(),
                                    'Редактирование профиля',
                                    'Информация в вашем профиле не сервисе ' . config('app.url') . ' была успешно отредактирована.'
                                )
                            );

                        // Event
                        EventController::save(EventType::where(['const' => 'PROFILE_EDITED'])->first(), false);
                    }

                    return redirect()->back()->with('success', 'Данные обновлены.');

                }
            } else {

                return redirect()->back()
                    ->withErrors(['error' => $this->exception]);
            }
        }

        return view('profile/settings')
            ->with(
                [
                    'user' => Auth::user(),
                ]
            );
    }
}
