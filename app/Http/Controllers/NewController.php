<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewController extends Controller
{

    /**
     * News
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view(
            'news.index',
            [
                'items' => $this->getNews(),
            ]
        );
    }

    /**
     * News data
     *
     * @return mixed
     */
    private function getNews()
    {
        return News::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
            ]
        )
            ->orderBy('created_at', 'DESC')
            ->limit($this->limit)
            ->offset($this->offset)
            ->paginate();
    }

    /**
     * New
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function item(Request $request)
    {
        return view(
            'news.view',
            [
                'item' => $this->getNewItem($request->slug),
            ]
        );
    }

    /**
     * New data
     *
     * @param $slug
     *
     * @return mixed
     */
    private function getNewItem($slug)
    {
        return News::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
                'slug'    => $slug,
            ]
        )->first();
    }
}
