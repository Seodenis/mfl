<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{

    /**
     * Reviews
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view(
            'reviews.index',
            [
                'items' => $this->getReviews(),
            ]
        );
    }

    /**
     * Reviews data
     *
     * @return mixed
     */
    private function getReviews()
    {
        return Review::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
            ]
        )
            ->orderBy('created_at', 'DESC')
            ->limit($this->limit)
            ->offset($this->offset)
            ->paginate();
    }

    /**
     * Review
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function item(Request $request)
    {
        return view(
            'reviews.view',
            [
                'item' => $this->getReview($request->slug),
            ]
        );
    }

    /**
     * Review data
     *
     * @param $slug
     *
     * @return mixed
     */
    private function getReview($slug)
    {
        return Review::where(
            [
                'active'  => 1,
                'site_id' => $this->site_id,
                'slug'    => $slug,
            ]
        )->first();
    }
}