<?php

namespace App\Http\Controllers;

use App\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{

    /**
     * Partners
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view(
            'partners.index',
            [
                'items' => $this->getPartners(),
            ]
        );
    }

    /**
     * Partners data
     *
     * @return mixed
     */
    private function getPartners()
    {
        return Partner::where(
            [
                'active' => 1,
            ]
        )
            ->orderBy('order', 'ASC')
            /*->limit($this->limit)
            ->offset($this->offset)
            ->paginate();*/
            ->get();
    }
}
