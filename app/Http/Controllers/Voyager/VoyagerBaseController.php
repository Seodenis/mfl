<?php

namespace App\Http\Controllers\Voyager;

use App\ArticleCat;
use App\Specialization;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseVoyagerBaseController;

class VoyagerBaseController extends BaseVoyagerBaseController
{

    private $uri;

    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        $this->getUri($request, $dataType);

        $request['uri'] = $this->uri;

        if ( ! $request->ajax()) {
            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            event(new BreadDataUpdated($dataType, $data));

            return redirect()
                //->route("voyager.{$dataType->slug}.index")
                ->back()
                ->with(
                    [
                        'message'    => __(
                                'voyager::generic.successfully_updated'
                            ) . " {$dataType->display_name_singular}",
                        'alert-type' => 'success',
                    ]
                );
        }
    }

    private function getUri($data, $model)
    {
        if ($model->front_slug) {

            // It`s only for services
            if (json_decode($model->front_slug)) {

                $data->hot = $data->hot ? 1 : 0;

                foreach (json_decode($model->front_slug) as $k => $v) {
                    if ($data->hot == $v->hot) {
                        $model->front_slug = $v->slug;
                    }
                }
            }

            $this->uri = $model->front_slug . DIRECTORY_SEPARATOR;

            if (isset($data->parent_id)) {

                if ( ! $parent = ArticleCat::where(['id' => $data->parent_id])->first()) {
                    $parent = Specialization::where(['id' => $data->parent_id])->first();
                }

                if ($parent) {
                    $this->uri .= $parent->slug . DIRECTORY_SEPARATOR;
                }
            }

            $this->uri .= $data->slug . '.html';
        }
    }

    /**
     * POST BRE(A)D - Store data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if ( ! $request->has('_validate')) {

            $this->getUri((object)$request->all(), $dataType);

            $request['uri'] = $this->uri;

            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

            event(new BreadDataAdded($dataType, $data));

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with(
                    [
                        'message'    => __(
                                'voyager::generic.successfully_added_new'
                            ) . " {$dataType->display_name_singular}",
                        'alert-type' => 'success',
                    ]
                );
        }
    }
}
