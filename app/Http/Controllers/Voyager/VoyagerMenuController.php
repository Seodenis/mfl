<?php

namespace App\Http\Controllers\Voyager;

use App\MenuItemSite;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerMenuController as BaseVoyagerMenuController;

class VoyagerMenuController extends BaseVoyagerMenuController
{

    public function add_item(Request $request)
    {
        $menu = Voyager::model('Menu');

        $this->authorize('add', $menu);

        $data = $this->prepareParameters(
            $request->all()
        );

        unset($data['id']);
        $data['order'] = Voyager::model('MenuItem')->highestOrderMenuItem();

        // Check if is translatable
        $_isTranslatable = is_bread_translatable(Voyager::model('MenuItem'));
        if ($_isTranslatable) {
            // Prepare data before saving the menu
            $trans = $this->prepareMenuTranslations($data);
        }

        $data['registered'] = isset($data['registered']) ? 1 : 0;

        $siteIds = (isset($data['sites'])) ? $data['sites'] : false;
        unset($data['sites']);

        $menuItem = Voyager::model('MenuItem')->create($data);

        // Set site ids
        if ($siteIds) {
            foreach ($siteIds as $k => $v) {
                $item               = new MenuItemSite();
                $item->site_id      = $v;
                $item->menu_item_id = $menuItem->id;

                $item->save();
            }
        }

        // Save menu translations
        if ($_isTranslatable) {
            $menuItem->setAttributeTranslations('title', $trans, true);
        }

        return redirect()
            ->route('voyager.menus.builder', [$data['menu_id']])
            ->with(
                [
                    'message'    => __('voyager::menu_builder.successfully_created'),
                    'alert-type' => 'success',
                ]
            );
    }

    public function update_item(Request $request)
    {
        $id   = $request->input('id');
        $data = $this->prepareParameters(
            $request->except(['id'])
        );

        $menuItem = Voyager::model('MenuItem')->findOrFail($id);

        $this->authorize('edit', $menuItem->menu);

        if (is_bread_translatable($menuItem)) {
            $trans = $this->prepareMenuTranslations($data);

            // Save menu translations
            $menuItem->setAttributeTranslations('title', $trans, true);
        }

        $data['registered'] = isset($data['registered']) ? 1 : 0;

        $siteIds = (isset($data['sites'])) ? $data['sites'] : false;
        unset($data['sites']);

        $menuItem->update($data);

        MenuItemSite::where(['menu_item_id' => $menuItem->id])
            ->delete();

        // Set site ids
        if ($siteIds) {
            foreach ($siteIds as $k => $v) {
                $item               = new MenuItemSite();
                $item->site_id      = $v;
                $item->menu_item_id = $menuItem->id;

                $item->save();
            }
        }

        return redirect()
            ->route('voyager.menus.builder', [$menuItem->menu_id])
            ->with(
                [
                    'message'    => __('voyager::menu_builder.successfully_updated'),
                    'alert-type' => 'success',
                ]
            );
    }

    /**
     * Isset site id in menu item
     *
     * @param $siteId
     * @param $menuId
     *
     * @return mixed
     */
    public function siteIdIsset($siteId, $menuId)
    {
        //mail('my-gebbels@yandex.ru', 111, $siteId . ' - ' . $menuId);

        if (MenuItemSite::where(
            [
                'site_id'      => $siteId,
                'menu_item_id' => $menuId,
            ]
        )->first()) {
            echo true;
        } else {
            echo false;
        }
    }
}
