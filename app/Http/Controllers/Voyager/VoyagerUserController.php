<?php

namespace App\Http\Controllers\Voyager;

use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class VoyagerUserController extends VoyagerBreadController
{

    /**
     * Excel download
     *
     * @param $data
     *
     * @return mixed
     */
    public function excelDownload(Request $request)
    {
        return Excel::create(
            'users-'.date('d.m.Y H:i:s'),
            function ($excel) {

                $excel->sheet(
                    'mySheet',
                    function ($sheet) {
                        $sheet->loadView('voyager::users.excel-download')
                            ->with('items', User::all());
                    }
                );
            }
        )->download();
    }
}