<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventType;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{

    /**
     * Save event
     *
     * @param EventType $type
     * @param           $title
     *
     * @return bool
     */
    public static function save(EventType $type, $title, $userId = false, $objectId = false)
    {
        try {
            $item          = new Event();
            $item->type_id = $type->id;
            $item->site_id = static::$siteId;
            $item->user_id = ($userId) ? $userId : Auth::user()->id;
            if ($objectId != false) {
                $item->object_id = $objectId;
            }
            $item->title = $title;

            return $item->save();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
}