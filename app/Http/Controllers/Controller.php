<?php

namespace App\Http\Controllers;

use App\Content;
use App\MetaData;
use App\MetaDataRoute;
use App\Traits\Site;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Site;

    protected static $siteId;
    protected $limit = 20;
    protected $offset = 0;
    protected $items;
    protected $exception = false;
    protected $success = false;
    protected $item;
    protected $userFolder = false;
    protected $metaData;

    public function __construct()
    {
        $this->getSiteId();
        static::$siteId = $this->site_id;

        $this->setFolder();

        // Metadata
        $this->metaData = $this->metaData(request());
        View::share('metaData', $this->metaData);
    }

    /**
     * If not exist make user folder
     *
     * @author ansotov
     */
    protected function setFolder()
    {
        if (Auth::check()) {
            $path = user_folder_path(md5(Auth::user()->id));
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            $this->userFolder = $path;
        }

    }

    /**
     * Metadata of main parts
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    private function metaData(Request $request)
    {
        if ($route = MetaDataRoute::whereConst($request->route()->getName())->first()) {
            return MetaData::where(
                [
                    'site_id'  => $this->site_id,
                    'route_id' => $route->id
                ]
            )->first();
        } else {
            return false;
        }
    }

    /**
     * Content by slug
     *
     * @param \Illuminate\Support\Facades\Request $request
     * @param                                     $slug
     *
     * @return mixed
     */
    protected function getContentData(Request $request, $slug)
    {
        // Offset
        if ($request->get('page')) {
            $this->offset = ($request->get('page') - 1) * $this->limit;
        }

        return Content::select(['contents.*'])
            ->join('contents AS cs', 'cs.id', '=', 'contents.parent_id')
            ->where('cs.slug', '=', trim($slug))
            ->where('contents.published', '=', 1)
            ->where('contents.site_id', '=', $this->site_id)
            ->orderBy('contents.id', 'DESC')
            ->limit($this->limit)
            ->offset($this->offset)
            ->paginate();
    }
}
