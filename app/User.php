<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class User extends \TCG\Voyager\Models\User
{
    protected $connection = 'pockethelp';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'site_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get free lawyer
     *
     * @return mixed
     */
    public static function getEmptyLawyer()
    {
        return User::from('users AS u')
            ->select('u.id', 'u.email', 'u.name')
            ->selectRaw('(SELECT count(id) FROM requests WHERE lawyer_id = u.id AND closed = 0) as count_requests')
            ->join('roles AS r', 'r.id', '=', 'u.role_id')
            ->where('r.name', '=', 'lawyer')
            ->groupBy('u.id')
            ->orderBy('count_requests')
            ->first();
    }
}
