<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainArticleType extends Model
{
    public $timestamps = false;
}
