<?php

namespace App\Widgets;

use App\Partner;
use Arrilot\Widgets\AbstractWidget;

class Partners extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view(
            'widgets.partners',
            [
                'items'  => $this->getData(),
                'config' => $this->config,
            ]
        );
    }

    /**
     * Get data
     *
     * @return bool
     */
    private function getData()
    {
        return Partner::select('*')
            ->where('active', '=', 1)
            ->limit(5)
            ->orderBy('id', 'DESC')
            ->get();
    }
}
