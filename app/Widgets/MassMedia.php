<?php

namespace App\Widgets;

use App\MassMediaArticle;
use Arrilot\Widgets\AbstractWidget;

class MassMedia extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view(
            'widgets.mass_media',
            [
                'items' => $this->getData(),
            ]
        );
    }

    /**
     * Get data
     *
     * @return bool
     */
    private function getData()
    {
        return \App\MassMedia::where('active', '=', 1)
            //->where('site_id', '=', $this->site_id)
            //->limit(5)
            ->orderBy('id', 'DESC')
            ->get();
    }
}
