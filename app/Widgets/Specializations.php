<?php

namespace App\Widgets;

use App\Specialization;
use Arrilot\Widgets\AbstractWidget;

class Specializations extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view(
            'widgets.specializations',
            [
                'items' => $this->getData(),
            ]
        );
    }

    /**
     * Get data
     *
     * @return bool
     */
    private function getData()
    {
        return Specialization::select('*')
            ->where('active', '=', 1)
            ->where('site_id', '=', $this->site_id)
            ->orderBy('order', 'ASC')
            ->get();
    }
}
