<?php

namespace App\Widgets;

use App\Content;
use Arrilot\Widgets\AbstractWidget;

class Services extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view(
            'widgets.services',
            [
                'parents' => $this->getParentData(),
            ]
        );
    }

    /**
     * Items by parent id
     *
     * @param $parentId
     *
     * @return mixed
     */
    public static function getItemsByParent($parentId)
    {
        return Content::select('*')
            ->where('parent_id', '=', $parentId)
            ->where('in_widget', '=', 1)
            ->get();
    }

    /**
     * Parent items
     *
     * @return mixed
     */
    private function getParentData()
    {
        $main = Content::where('slug', '=', trim($this->config['slug']))
            ->where('site_id', '=', $this->site_id)
            ->first();

        return Content::select('contents.*')
            ->where('parent_id', '=', $main->id)
            ->where('site_id', '=', $this->site_id)
            ->get();
    }
}
