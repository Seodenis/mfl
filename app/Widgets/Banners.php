<?php

namespace App\Widgets;

use App\Banner;
use Arrilot\Widgets\AbstractWidget;

class Banners extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view(
            'widgets.banners',
            [
                'items' => $this->getData(),
            ]
        );
    }

    /**
     * Get data
     *
     * @return bool
     */
    private function getData()
    {
        if ($this->config['type']) {
            return Banner::select(
                [
                    'banners.title',
                    'banners.text',
                    'banners.button_title',
                    'banners.button_url',
                    'banners.src'
                ]
            )
                ->join('banner_types AS bt', 'bt.id', '=', 'banners.type_id')
                ->where('bt.const', '=', trim($this->config['type']))
                ->where('banners.active', '=', 1)
                ->where('banners.site_id', '=', $this->site_id)
                ->orderBy('banners.order', 'ASC')
                ->get();
        } else {
            return false;
        }
    }
}
