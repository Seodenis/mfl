<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class Menu extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    // Return data
    private $items, $positionType;

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $this->positionType = isset($this->config['positionType']) ? $this->config['positionType'] : 'top';

        return view(
            'widgets.menu.' . $this->positionType,
            [
                'items' => $this->getData(),
            ]
        );
    }

    /**
     * Get data
     *
     * @return bool
     */
    private function getData()
    {
        return menu(trim($this->config['type']), 'frontItems', []);
    }
}
