<?php

namespace App\Widgets;

use App\WonCase;
use Arrilot\Widgets\AbstractWidget;

class WonCases extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view(
            'widgets.won_cases',
            [
                'items' => $this->getData(),
            ]
        );
    }

    /**
     * Get data
     *
     * @return bool
     */
    private function getData()
    {
        return WonCase::select('*')
            ->where('active', true)
            ->where('in_module', true)
            ->whereRaw('(site_id = ' . $this->site_id . ' or site_id is null)')
            ->orderBy('order', 'DESC')
            ->get();
    }
}
