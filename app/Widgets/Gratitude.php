<?php

namespace App\Widgets;

use App\Review;
use Arrilot\Widgets\AbstractWidget;

class Gratitude extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view(
            'widgets.gratitude',
            [
                'items' => $this->getData(),
            ]
        );
    }

    /**
     * Get data
     *
     * @return bool
     */
    private function getData()
    {
        return Review::select('*')
            ->where('active', '=', 1)
            ->where('site_id', '=', $this->site_id)
            ->limit(5)
            ->orderBy('id', 'DESC')
            ->get();
    }
}
