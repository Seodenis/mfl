<?php

namespace App\Traits;
use Illuminate\Support\Facades\View;

trait Site
{
    // Site id
    protected $site_id = 0;

    protected $site;

    public function getSiteId()
    {
        $this->site = $this->getDomain();

        $this->site_id = $this->site ? $this->site->id : 1;

        View::share('site', $this->site);
    }

    /**
     * Get domain
     * @return mixed
     */
    protected function getDomain()
    {
        $const = sub_domain() ? sub_domain() : 'web';

        return \App\Site::where('const', $const)->first();
    }
}
