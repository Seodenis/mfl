<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestValue extends Model
{
    protected $connection = 'pockethelp';
    public $timestamps = false;
}
