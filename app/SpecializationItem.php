<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecializationItem extends Model
{
    /**
     * Category
     *
     * @author ansotov
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Specialization', 'parent_id', 'id');
    }
}
