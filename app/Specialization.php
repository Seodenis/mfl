<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{

    /**
     * Items
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function itemsData()
    {
        return $this->hasMany('App\SpecializationItem', 'parent_id', 'id')
            ->orderBy('order')
            ->limit(4);
    }

    /**
     * Items full
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function itemsFullData()
    {
        return $this->hasMany('App\SpecializationItem', 'parent_id', 'id')
            ->orderBy('order');
    }
}
