<?php


use Illuminate\Support\Carbon;

if ( ! function_exists('domain')) {
    function domain()
    {
        return $_SERVER['HTTP_HOST'];
    }
}

// Only for 3 level subdomain
if ( ! function_exists('sub_domain')) {
    function sub_domain()
    {
        $domain = explode('.', $_SERVER['HTTP_HOST']);

        if (count($domain) == 3) {
            return $domain[0];
        } else {
            return false;
        }
    }
}

if ( ! function_exists('dateFormat')) {
    /**
     * Get the path to the user folder.
     *
     * @param  string $path
     *
     * @return string
     */
    function dateFormat($date, $format = 'd.m.Y')
    {
        return Carbon::parse($date)->format($format);
    }
}

// Description
if ( ! function_exists('description')) {
    function description($description, $text, $length = 350)
    {
        if ($description) {
            return trim($description);
        } else {
            return str_limit(strip_tags($text), $length);
        }
    }
}

// Front storage path
if ( ! function_exists('front_storage_path')) {
    /**
     * Get the path to the storage folder.
     *
     * @param  string $path
     *
     * @return string
     */
    function front_storage_path($path = '')
    {
        return DIRECTORY_SEPARATOR . 'storage' . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}

// Front storage path
if ( ! function_exists('avatarPath')) {
    /**
     * Get the path to the storage folder.
     *
     * @param  string $path
     *
     * @return string
     */
    function avatarPath($path = '')
    {
        if (file_exists(base_path($path))) {
            return Storage::disk(config('voyager.storage.disk'))->url($path);
        } else {
            return config('app.phUrl') . front_storage_path($path);
        }
    }
}

// Front storage path
if ( ! function_exists('bannerImage')) {
    /**
     * Get the path to the storage folder.
     *
     * @param  string $path
     *
     * @return string
     */
    function bannerImage($path = '')
    {
        return $path ? $path : 'banners' . DIRECTORY_SEPARATOR . 'banner_bg_default.jpg';
    }
}

if ( ! function_exists('user_folder_front_path')) {
    /**
     * Get the path to the user folder.
     *
     * @param  string $path
     *
     * @return string
     */
    function user_folder_front_path($path = '')
    {
        return DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . ($path ? $path : false);
    }
}

if ( ! function_exists('user_folder_path')) {
    /**
     * Get the path to the user folder.
     *
     * @param  string $path
     *
     * @return string
     */
    function user_folder_path($path = '')
    {
        return app('path.storage') . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . ($path ? $path : false);
    }
}

if ( ! function_exists('is_image')) {
    function is_image($mimeType)
    {
        $extensions = [
            'image/jpg',
            'image/jpe',
            'image/jpeg',
            'image/jfif',
            'image/png',
            'image/bmp',
            'image/dib',
            'image/gif',
        ];

        if (in_array($mimeType, $extensions)) {
            return true;
        } else {
            return false;
        }
    }
}

if ( ! function_exists('get_filesize')) {
    /**
     * Get file size
     *
     * @param $file
     *
     * @return string
     */
    function get_filesize($file)
    {
        // идем файл
        if ( ! file_exists($file)) {
            return "Файл  не найден";
        }
        // теперь определяем размер файла в несколько шагов
        $filesize = filesize($file);
        // Если размер больше 1 Кб
        if ($filesize > 1024) {
            $filesize = ($filesize / 1024);
            // Если размер файла больше Килобайта
            // то лучше отобразить его в Мегабайтах. Пересчитываем в Мб
            if ($filesize > 1024) {
                $filesize = ($filesize / 1024);
                // А уж если файл больше 1 Мегабайта, то проверяем
                // Не больше ли он 1 Гигабайта
                if ($filesize > 1024) {
                    $filesize = ($filesize / 1024);
                    $filesize = round($filesize, 1);

                    return $filesize . " ГБ";
                } else {
                    $filesize = round($filesize, 1);

                    return $filesize . " MБ";
                }
            } else {
                $filesize = round($filesize, 1);

                return $filesize . " Кб";
            }
        } else {
            $filesize = round($filesize, 1);

            return $filesize . " байт";
        }
    }
}
