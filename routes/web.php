<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get(
    '/clear',
    function () {
        Artisan::call('cache:clear');
        Artisan::call('config:cache');
        Artisan::call('view:clear');
        Artisan::call('route:clear');

        return "Кэш очищен.";
    }
);

Route::get('/', 'MainController@index')->name('main');
Route::get('/test', 'MainController@test')->name('test');

// Sitemap.xml
Route::get('sitemap.xml', 'MainController@sitemap')->name('sitemap');

Route::group(
    ['prefix' => 'admin', 'middleware' => 'auth'],
    function () {
        Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
        Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');

        Route::match(['get', 'post'], '/users/excel-download', 'Voyager\VoyagerUserController@excelDownload')->name('voyager.users.excel-download');
        Voyager::routes();
    }
);

// Profile
Route::group(
    ['prefix' => 'profile', 'middleware' => 'auth'],
    function () {
        Route::get('/', 'ProfileController@index')->name('profile');
        Route::get('/requests', 'ProfileController@requests')->name('profile.requests');
        Route::get('/requests/{id}', 'ProfileController@requestItem')->name('profile.requests.item');
        Route::match(['get', 'post'], '/settings', 'ProfileController@settings')->name('profile.settings');
    }
);

// Requests
Route::group(
    ['prefix' => 'requests'],
    function () {
        Route::match(['get', 'post'], '/add/{type?}', 'RequestController@addItem')->name('requests.add');
        Route::match(['get', 'post'], '/item/{requestId}', 'RequestController@getItem')->name('requests.get-item');
    }
);

Route::group(
    ['prefix' => 'requests', 'middleware' => 'auth'],
    function () {
        Route::match(['get', 'post'], '/', 'RequestController@index')->name('requests');
        Route::get('/show/{id}', 'RequestController@show')->name('requests.show');
        Route::get('/chat/{requestId}', 'RequestController@chat')->name('requests.chat');
    }
);

// Payment
Route::group(
    ['prefix' => 'payment', 'middleware' => 'auth'],
    function () {
        Route::match(['get', 'post'], '/pay', 'PaymentController@pay')->name('payment.pay');
    }
);

Route::group(
    ['prefix' => 'payment'],
    function () {
        Route::match(['get', 'post'], '/check', 'PaymentController@check')->name('payment.check');
    }
);

// Check data
Route::post('/check-isset-menu-item-site/{siteId}/{menuItem}', 'Voyager\VoyagerMenuController@siteIdIsset')->name('voyagerMenu.siteIdIsset');

// Articles
Route::get('/articles.html', 'ArticleController@index')->name('article.index');
Route::get('/articles/{catSlug}.html', 'ArticleController@category')->name('article.category');
Route::get('/articles/{catSlug}/{slug}.html', 'ArticleController@item')->name('article.item');

// Price
Route::get('/prajs-list.html', 'ServiceController@index')->name('services.index');
Route::get('/prajs-list/{slug}.html', 'ServiceController@item')->name('services.item');

// Hot services
Route::get('/goryachie-uslugi.html', 'ServiceController@hot')->name('services.hot');
Route::get('/goryachie-uslugi/{slug}.html', 'ServiceController@hotItem')->name('services.hot.item');

// News
Route::get('/novosti.html', 'NewController@index')->name('news.index');
Route::get('/novosti/{slug}.html', 'NewController@item')->name('news.item');

// Reviews
Route::get('/otzyivy.html', 'ReviewController@index')->name('reviews.index');
Route::get('/otzyivy/{slug}.html', 'ReviewController@item')->name('reviews.item');

// Partners
Route::get('/nashi-partnery.html', 'PartnerController@index')->name('partners.index');

// Specializations
Route::get('/uslugi.html', 'SpecializationController@index')->name('specializations.index');
Route::get('/uslugi/{slug}.html', 'SpecializationController@item')->name('specializations.item');
Route::get('/uslugi/{catSlug}/{slug}.html', 'SpecializationController@dataItem')->name('specializations.items.item');

// Answers
Route::get('/vopros-otvet.html', 'AnswerController@index')->name('answers.index');
Route::get('/vopros-otvet/{slug}.html', 'AnswerController@item')->name('answers.item');

// Mass media
Route::get('/smi.html', 'MassMediaArticleController@index')->name('mass_media_articles.index');
Route::get('/smi/{slug}.html', 'MassMediaArticleController@item')->name('mass_media_articles.item');

// Won cases
Route::get('/vyiigrannyie-dela.html', 'WonCaseController@index')->name('won_cases.index');
Route::get('/vyiigrannyie-dela/{slug}.html', 'WonCaseController@item')->name('won_cases.item');

// About
Route::get('/o-kompanii/komanda.html', 'AboutController@employees')->name('about.employees.index');
Route::get('/o-kompanii/komanda/{slug}.html', 'AboutController@itemEmployee')->name('about.employees.item');

// Seo
Route::group(
    ['prefix' => 'seo'],
    function () {
        Route::get('/slug', 'SeoController@slug')->name('seo.slug');
    }
);

// Main articles
Route::get('/', 'MainController@index')->name('main');
//Route::get('/test', 'MainController@test')->name('test');
Route::get('/o-kompanii.html', 'MainController@about')->name('about');
Route::get('/kontaktyi.html', 'MainController@contacts')->name('contacts');
Route::get('/kak-my-rabotaem.html', 'MainController@work')->name('work');
Route::get('/payment.html', 'MainController@payment')->name('payment');
Route::get('/press-czentr.html', 'MainController@pressCenter')->name('press-center');
Route::get('/privilege.html', 'MainController@privilege')->name('privilege');

Route::get('/main-article/{slug}', 'MainController@mainArticle')->name('single-main-article');

Route::match(['get', 'post'], '/message/add', 'MessageController@add')->name('message.add');
Route::match(['get', 'post'], '/messages/my', 'MessageController@unreadItems')->name('message.my');
Route::match(['get', 'post'], '/messages/my-alerts', 'MessageController@unreadChanges')->name('message.my-alerts');
Route::match(['get', 'post'], '/messages/reminder', 'MessageController@reminder')->name('message.reminder');
Route::match(['get', 'post'], '/messages/{requestId}', 'MessageController@index')->name('message');

Route::get(
    '/{any?}',
    function () {
        abort(404);
    }
);
