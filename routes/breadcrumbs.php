<?php

// main
Breadcrumbs::for(
    'main',
    function ($trail) {
        $trail->push('Главная', route('main'));
    }
);

// main > about
Breadcrumbs::for(
    'about',
    function ($trail) {
        $trail->parent('main');
        $trail->push('О компании', route('about'));
    }
);

// main > specializations
Breadcrumbs::for(
    'specializations.index',
    function ($trail) {
        $trail->parent('main');
        $trail->push('Специализации', route('specializations.index'));
    }
);

// main > specializations > specialization item
Breadcrumbs::for(
    'specializations.item',
    function ($trail, $item) {
        $trail->parent('specializations.index');
        $trail->push($item->short_title, route('specializations.item', $item->slug));
    }
);

// main > specializations > specialization item > item
Breadcrumbs::for(
    'specializations.items.item',
    function ($trail, $item) {
        $trail->parent('specializations.item', $item->category);
        $trail->push($item->title, route('specializations.items.item', ['catSlug' => $item->category->slug, 'slug' => $item->slug]));
    }
);

// main > services
Breadcrumbs::for(
    'services.index',
    function ($trail) {
        $trail->parent('main');
        $trail->push('Прайс-лист', route('services.index'));
    }
);

// main > services
Breadcrumbs::for(
    'services.item',
    function ($trail, $item) {
        $trail->parent('services.index');
        $trail->push($item->title, route('services.item', ['slug' => $item->slug]));
    }
);

// main > services hot
Breadcrumbs::for(
    'services.hot',
    function ($trail) {
        $trail->parent('main');
        $trail->push('Горящие услуги', route('services.hot'));
    }
);

// main > articles
Breadcrumbs::for(
    'article.index',
    function ($trail) {
        $trail->parent('main');
        $trail->push('Статьи', route('article.index'));
    }
);

// main > articles > category
Breadcrumbs::for(
    'article.category',
    function ($trail, $item) {
        $trail->parent('article.index');
        $trail->push($item->title, route('article.category', ['slug' => $item->slug]));
    }
);

// main > articles > category > item
Breadcrumbs::for(
    'article.item',
    function ($trail, $category, $item) {
        $trail->parent('article.category', $category);
        $trail->push($item->title, route('article.item', ['catSlug' => $category->slug, 'slug' => $item->slug]));
    }
);

// main > mass media
Breadcrumbs::for(
    'mass_media_articles.index',
    function ($trail) {
        $trail->parent('main');
        $trail->push('Мы в СМИ', route('mass_media_articles.index'));
    }
);

// main > mass media > item
Breadcrumbs::for(
    'mass_media_articles.item',
    function ($trail, $item) {
        $trail->parent('mass_media_articles.index');
        $trail->push($item->title, route('mass_media_articles.item', ['slug' => $item->slug]));
    }
);

// main > news
Breadcrumbs::for(
    'news.index',
    function ($trail) {
        $trail->parent('main');
        $trail->push('Юридические новости', route('news.index'));
    }
);

// main > news > item
Breadcrumbs::for(
    'news.item',
    function ($trail, $item) {
        $trail->parent('news.index');
        $trail->push($item->title, route('news.item', ['slug' => $item->slug]));
    }
);

// main > reviews
Breadcrumbs::for(
    'reviews.index',
    function ($trail) {
        $trail->parent('main');
        $trail->push('Отзывы', route('reviews.index'));
    }
);

// main > reviews > item
Breadcrumbs::for(
    'reviews.item',
    function ($trail, $item) {
        $trail->parent('reviews.index');
        $trail->push($item->title, route('reviews.item', ['slug' => $item->slug]));
    }
);

// main > answers
Breadcrumbs::for(
    'answers.index',
    function ($trail) {
        $trail->parent('main');
        $trail->push('Вопросы-ответы', route('answers.index'));
    }
);

// main > answers > item
Breadcrumbs::for(
    'answers.item',
    function ($trail, $item) {
        $trail->parent('answers.index');
        $trail->push($item->title, route('answers.item', ['slug' => $item->slug]));
    }
);

// main > answers
Breadcrumbs::for(
    'won_cases.index',
    function ($trail) {
        $trail->parent('main');
        $trail->push('Выигранные дела', route('won_cases.index'));
    }
);

// main > about > employees
Breadcrumbs::for(
    'about.employees.index',
    function ($trail) {
        $trail->parent('main');
        $trail->push('Наша команда', route('about.employees.index'));
    }
);

// main > about > employees > item
Breadcrumbs::for(
    'about.employees.item',
    function ($trail, $item) {
        $trail->parent('about.employees.index');
        $trail->push($item->title, route('about.employees.item', ['slug' => $item->slug]));
    }
);

// main > article
Breadcrumbs::for(
    'article.data',
    function ($trail, $route, $item) {
        $trail->parent('main');
        $trail->push($item->title, route($route, ['slug' => $item->slug]));
    }
);

// main > partners
Breadcrumbs::for(
    'partners.index',
    function ($trail) {
        $trail->parent('main');
        $trail->push('Наши партнеры', route('partners.index'));
    }
);

// main > profile > requests
Breadcrumbs::for(
    'profile.requests',
    function ($trail) {
        $trail->parent('main');
        $trail->push('Заявки', route('profile.requests'));
    }
);


// main > profile > request
Breadcrumbs::for(
    'profile.requests.item',
    function ($trail, $item) {
        $trail->parent('profile.requests');
        $trail->push('Заявкa №' . $item->id, route('profile.requests.item', ['id' => $item->id]));
    }
);

// main > profile > settings
Breadcrumbs::for(
    'profile.settings',
    function ($trail) {
        $trail->parent('main');
        $trail->push('Настройки', route('profile.settings'));
    }
);
