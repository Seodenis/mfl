@extends('layouts.email')

@section('content')
    {{-- Intro Lines --}}
    @foreach ($introLines as $line)
        {!! $line !!}

    @endforeach

    {{-- Action Button --}}
    @isset($actionText)
        <?php
        switch ($level) {
            case 'success':
                $color = 'green';
                break;
            case 'error':
                $color = 'red';
                break;
            default:
                $color = 'blue';
        }
        ?>
        @component('mail::button', ['url' => $actionUrl, 'color' => $color])
            {{ $actionText }}
        @endcomponent
    @endisset

    {{-- Subcopy --}}
    @isset($actionText)
        @component('mail::subcopy')

        @endcomponent
    @endisset
@endsection
