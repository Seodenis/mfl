<?php

namespace Illuminate\Auth\Notifications;

use App\MailTemplate;
use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use App\Http\Traits\MailTemplate as MTemplate;

class ResetPassword extends Notification
{
    use MTemplate;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string $token
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure $callback
     *
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed $notifiable
     *
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }

        $this->getTemplate('RESET_PASSWORD');

        if ($this->mailTemplate) {

            return (new MailMessage)
                ->subject($this->mailTemplate->subject)
                ->line($this->mailTemplate->message)
                ->action(Lang::getFromJson('Смена пароля'), url(config('app.url').route('password.reset', $this->token, false)));
        } else {

            return (new MailMessage)
                ->subject(Lang::getFromJson('Смена пароля'))
                ->line(Lang::getFromJson('Вы запросили смену пароля.'))
                ->action(Lang::getFromJson('Смена пароля'), url(config('app.url').route('password.reset', $this->token, false)))
                ->line(Lang::getFromJson('Если Вы не запрашивали смену пароля, то просто проигнорируйте это письмо.'));

        }
    }
}
