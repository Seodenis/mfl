<?php

namespace Arrilot\Widgets;

use App\Content;
use App\Traits\Site;

abstract class AbstractWidget
{
    use Site;

    /**
     * The number of seconds before each reload.
     * False means no reload at all.
     *
     * @var int|float|bool
     */
    public $reloadTimeout = false;

    /**
     * The number of minutes before cache expires.
     * False means no caching at all.
     *
     * @var int|float|bool
     */
    public $cacheTime = false;

    /**
     * Cache tags allow you to tag related items in the cache and then flush all cached values that assigned a given
     * tag.
     *
     * @var array
     */
    public $cacheTags = [];

    /**
     * Should widget params be encrypted before sending them to /arrilot/load-widget?
     * Turning encryption off can help with making custom reloads from javascript, but makes widget params publicly
     * accessible.
     *
     * @var bool
     */
    public $encryptParams = true;

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        foreach ($config as $key => $value) {
            $this->config[$key] = $value;
        }

        $this->getSiteId();
    }

    /**
     * Placeholder for async widget.
     * You can customize it by overwriting this method.
     *
     * @return string
     */
    public function placeholder()
    {
        return '';
    }

    /**
     * Async and reloadable widgets are wrapped in container.
     * You can customize it by overriding this method.
     *
     * @return array
     */
    public function container()
    {
        return [
            'element'    => 'div',
            'attributes' => 'style="display:inline" class="arrilot-widget-container"',
        ];
    }

    /**
     * Cache key that is used if caching is enabled.
     *
     * @param $params
     *
     * @return string
     */
    public function cacheKey(array $params = [])
    {
        return 'arrilot.widgets.'.serialize($params);
    }

    /**
     * Cache tags to help flush all cache with the same tag(s).
     *
     * @return array
     */
    public function cacheTags()
    {
        return array_unique(array_merge(['widgets'], $this->cacheTags));
    }

    /**
     * Add defaults to configuration array.
     *
     * @param array $defaults
     */
    protected function addConfigDefaults(array $defaults)
    {
        $this->config = array_merge($this->config, $defaults);
    }

    /**
     * Get data by slug
     *
     * @return bool
     */
    protected function getDataBySlug()
    {
        if ($this->config['slug']) {
            return Content::select(['contents.*'])
                ->join('contents AS cs', 'cs.id', '=', 'contents.parent_id')
                ->where('cs.slug', '=', trim($this->config['slug']))
                ->where('contents.published', '=', 1)
                ->where('contents.site_id', '=', $this->site_id)
                ->where('contents.in_widget', '=', 1)
                ->orderBy('contents.order', 'ASC')
                ->get();
        } else {
            return false;
        }
    }
}
